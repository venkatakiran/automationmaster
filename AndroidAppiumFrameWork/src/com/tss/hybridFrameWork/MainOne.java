package com.tss.hybridFrameWork;

//import test.java.NewTest;
import com.beust.testng.TestNG;

public class MainOne {

    public static void main(String[] args) {
        @SuppressWarnings("deprecation")
		TestNG testng = new TestNG();
         @SuppressWarnings("rawtypes")
		Class[] classes = new Class[]{DriverApp.class};
         testng.setTestClasses(classes);
         testng.run();
    }
}
