package com.tss.hybridFrameWork;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;

//import io.appium.java_client.ScrollsTo;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.tss.utils.OpenDialog;

public class KeywordDriveriOS {
	
	static final int IDENTIFYBY = 0;
	static final int SPECIAL_VALUE_FIELD=0;
	static final int URL =0;
	static final int LOCATOR = 1;
	static final int VALUE = 2;
	static final int XPATH1 = 1;
	static final int XPATH2 = 2;
	static final int XPATH3 = 3;
	
			
	
	
	public static void AcceptPopup(WebDriver driver, ArrayList<Object>  paramList){
		
		driver.switchTo().alert().accept();
		
		
	}
	
	public static void DismissPopup(WebDriver driver, ArrayList <Object>  paramList) {
		
		driver.switchTo().alert().dismiss();
		
	}
	
	
	public static void VerifyPopupText(WebDriver driver, ArrayList <Object>  paramList){
		String valuetoType = (String) paramList.get(SPECIAL_VALUE_FIELD);
		
		driver.switchTo().alert().getText().contains(valuetoType);
		
	}
	
	public static void wait(WebDriver driver, ArrayList <Object>  paramList){
		
		int waitTime = Integer.parseInt((String)paramList.get(SPECIAL_VALUE_FIELD));
		try {
			Thread.sleep(waitTime * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//driver.manage().timeouts().implicitlyWait(waitTime,TimeUnit.SECONDS);
	}
	
	public static void waitExplicit(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
			
		if (identifyBy.equalsIgnoreCase("id")){
		WebDriverWait wait = new WebDriverWait(driver, 20);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
	    }else if (identifyBy.equalsIgnoreCase("xpath")){
			WebDriverWait wait = new WebDriverWait(driver, 20);
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		    }else if (identifyBy.equalsIgnoreCase("class")){
		    	WebDriverWait wait = new WebDriverWait(driver, 20);
		    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
		    	}else if (identifyBy.equalsIgnoreCase("name")){
			    	WebDriverWait wait = new WebDriverWait(driver, 20);
			    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
			    	}
		}
	
	public static void waitForAbsence(WebDriver driver,ArrayList<Object> paramList) {
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoType = (String) paramList.get(VALUE);
		
		
		if (identifyBy.equalsIgnoreCase("id")) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(locator)));
		} else if (identifyBy.equalsIgnoreCase("xpath")) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
		} else if (identifyBy.equalsIgnoreCase("class")) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(locator)));
		} else if (identifyBy.equalsIgnoreCase("name")) {
			WebDriverWait wait = new WebDriverWait(driver, 30);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.name(locator)));
		}
	}

	public static void navigateto(WebDriver driver, ArrayList <Object>  paramList){
		driver.get((String)paramList.get(URL));
		driver.manage().window().maximize();
	}
	
	
	public static void clickButton(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){			
			driver.findElement(By.xpath(locator)).click();			
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("label")){
			driver.findElement(By.partialLinkText(locator)).click();
		}		
	}

	public static void clickLink(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("link")){
			driver.findElement(By.linkText(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("plink")){
			driver.findElement(By.partialLinkText(locator)).click();
		}
	}
	
	public static void clickIfExists(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		WebDriverWait wait = new WebDriverWait(driver, 5);	
				
		if (identifyBy.equalsIgnoreCase("xpath")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				driver.findElement(By.xpath(locator)).click();
			}catch(Exception E){
				System.out.println("The element doesn't exist on the page.");
			}
		}else if (identifyBy.equalsIgnoreCase("id")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
				driver.findElement(By.id(locator)).click();
			}catch(Exception E){
				System.out.println("The element doesn't exist on the page.");
			}
		}else if (identifyBy.equalsIgnoreCase("name")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.name(locator)));
				driver.findElement(By.name(locator)).click();
			}catch(Exception E){
				System.out.println("The element doesn't exist on the page.");
			}
		}else if (identifyBy.equalsIgnoreCase("class")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.className(locator)));
				driver.findElement(By.className(locator)).click();
		}catch(Exception E){
			System.out.println("The element doesn't exist on the page.");
		}
		}
	}
	
	public static void JSEClickElement(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			WebElement element = driver.findElement(By.xpath(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
			
		}else if (identifyBy.equalsIgnoreCase("id")){
			WebElement element = driver.findElement(By.id(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
			
		}else if (identifyBy.equalsIgnoreCase("name")){
			WebElement element = driver.findElement(By.name(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
		}
	}
	
	
	public static void JSETypeInEditBox(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException{
		//String identifyBy = (String) paramList.get(IDENTIFYBY);
		//String locator = (String) paramList.get(LOCATOR);
		
		{
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("tinyMCE.activeEditor.setContent('<h1>This is a simple description</h1> Testing')");
			
			/*Thread.sleep(3000);
			WebElement iframeItem = driver.findElement(By.xpath("//*[@id='item-description_ifr'] | //*[@id='location-description_ifr']"));
			driver.switchTo().frame(iframeItem);
			Thread.sleep(2000);
			
			WebElement editorbody = driver.findElement(By.className("mce-content-body"));			
			executor.executeScript("arguments[0].innerHTML = '<p>This is a sample text created by Kiran.<p>'", editorbody);
			
			driver.findElement(By.cssSelector("body")).sendKeys("your data");
			
			driver.switchTo().defaultContent();
			Thread.sleep(2000);*/
		}
	}
	
	
	public static void EnterText(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType);
		}
	}
	
	
	
	public static void EnterTextWOClear(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			//driver.findElement(By.xpath(locator)).click();
			//driver.findElement(By.xpath(locator)).clear();
			WebElement WE = driver.findElement(By.xpath(locator));
			WE.sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("name")){
			//driver.findElement(By.name(locator)).click();
			//driver.findElement(By.name(locator)).clear();
			WebElement WE=driver.findElement(By.name(locator));
			WE.sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("class")){
			//driver.findElement(By.className(locator)).click();
			//driver.findElement(By.className(locator)).clear();
			WebElement WE=driver.findElement(By.className(locator));
			WE.sendKeys(valuetoType);
		}
	}
	
	public static void EnterTextV3(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			//driver.findElement(MobileBy.xpath("locator")).setValue();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType);
		}
	}
	public static void EnterTextAndHitReturn(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType,Keys.RETURN);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType,Keys.RETURN);
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType, Keys.RETURN);
		}
	}
	
	public static void EnterTextAndHitTab(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType,Keys.TAB);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType,Keys.TAB);
		}
	}
	
	public static void isTextPresent(WebDriver driver, ArrayList<Object>  paramList) {
		String identifyBy = (String) paramList.get(SPECIAL_VALUE_FIELD);
		driver.findElement(By.xpath("//*[contains(.,'"+ identifyBy +"')]")).getAttribute("label");
			
	}
	
	public static void isWebTextPresent(WebDriver driver, ArrayList<Object>  paramList) throws Exception {
		String value = (String) paramList.get(SPECIAL_VALUE_FIELD);							
		Boolean result = driver.getPageSource().contains(value);			
		if(result == false){
			throw new Exception("***********Text doesn't match*************");			
		}else if(result == true){
			System.out.println("Success");
		}	
		}
	
	public static void verifyText(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		String valueLC=valuetoVerify.toLowerCase();		
		
		if (identifyBy.equalsIgnoreCase("xpath")){		
		String ElementName = driver.findElement(By.xpath(locator)).getAttribute("label");
		ElementName.toLowerCase();
		//System.out.println("****************************************"+ElementName+"**********************************************");
		String ElemName1= ElementName.replace("\u00A0", "");
		String RemoveSpace= ElemName1.replace(" ", "");
		String RemoveTabSpace=RemoveSpace.replace("\t", "").toLowerCase();
		String[] ElemName2 = RemoveTabSpace.split("");		
		String[] valueToVerify2 = valueLC.replace(" ", "").split("");		
		boolean result = Arrays.equals(valueToVerify2,ElemName2);				
		if(result == false){
            throw new Exception("************************************Text doesn't match**************************************");
        }
		}else if (identifyBy.equalsIgnoreCase("name")){		
    		String ElementName = driver.findElement(By.name(locator)).getAttribute("label");
    		ElementName.toLowerCase();
    		//System.out.println("****************************************"+ElementName+"**********************************************");
    		String ElemName1= ElementName.replace("\u00A0", "");
    		String RemoveSpace= ElemName1.replace(" ", "");
    		String RemoveTabSpace=RemoveSpace.replace("\t", "").toLowerCase();
    		String[] ElemName2 = RemoveTabSpace.split("");    		
    		String[] valueToVerify2 = valueLC.replace(" ", "").split("");    		
    		boolean result = Arrays.equals(valueToVerify2,ElemName2);				
    		if(result == false){
                throw new Exception("************************************Text doesn't match**************************************");
            }
		}	
	}
	
	public static void verifyTextPlain(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		valuetoVerify.trim().toLowerCase();	
		
		if (identifyBy.equalsIgnoreCase("xpath")){		
		String ElementName = driver.findElement(By.xpath(locator)).getAttribute("label");
		ElementName.toLowerCase();
		System.out.println("****************************************"+ElementName+"**********************************************");			
		String[] ElemName2 = ElementName.split("");				
		String[] valueToVerify2 = valuetoVerify.split("");		
		boolean result = Arrays.equals(valueToVerify2,ElemName2);				
		if(result == false){
            throw new Exception("************************************Text doesn't match**************************************");
        }
		}else if (identifyBy.equalsIgnoreCase("name")){			
			String ElementName = driver.findElement(By.name(locator)).getAttribute("label");
			ElementName.toLowerCase();
			System.out.println("****************************************"+ElementName+"**********************************************");				
			String[] ElemName2 = ElementName.split("");					
			String[] valueToVerify2 = valuetoVerify.split("");			
			boolean result = Arrays.equals(valueToVerify2,ElemName2);					
			if(result == false){
	            throw new Exception("************************************Text doesn't match**************************************");
	        }
			}
	}
	
	
	public static void verifyValue(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		valuetoVerify.trim().toLowerCase();	
		
		if (identifyBy.equalsIgnoreCase("xpath")){		
		String ElementName = driver.findElement(By.xpath(locator)).getAttribute("value");
		ElementName.trim().toLowerCase();
		System.out.println("****************************************"+ElementName+"**********************************************");			
		String[] ElemName2 = ElementName.split("");				
		String[] valueToVerify2 = valuetoVerify.split("");		
		boolean result = Arrays.equals(valueToVerify2,ElemName2);				
		if(result == false){
            throw new Exception("************************************Text doesn't match**************************************");
        }
		}else if (identifyBy.equalsIgnoreCase("name")){			
			String ElementName = driver.findElement(By.name(locator)).getAttribute("value");
			ElementName.trim().toLowerCase();
			System.out.println("****************************************"+ElementName+"**********************************************");				
			String[] ElemName2 = ElementName.split("");					
			String[] valueToVerify2 = valuetoVerify.split("");			
			boolean result = Arrays.equals(valueToVerify2,ElemName2);					
			if(result == false){
	            throw new Exception("************************************Text doesn't match**************************************");
	        }
			}
	}
	
	public static void verifyElement(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).isDisplayed();
		}
	}
	public static void verifyElementExist(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoVerify = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			Assert.assertTrue(driver.findElements(By.xpath(locator)).size()==1);
		}		
		else if (identifyBy.equalsIgnoreCase("id")){
			Assert.assertTrue(driver.findElements(By.id(locator)).size()==1);	
		}else if (identifyBy.equalsIgnoreCase("name")){
			Assert.assertTrue(driver.findElements(By.name(locator)).size()==1);	
		}
	
	}
	
	public static void verifyElementNotExist(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoVerify = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
		Assert.assertTrue(driver.findElements(By.xpath(locator)).size()<1);
		}		
		else if (identifyBy.equalsIgnoreCase("id")){
			Assert.assertTrue(driver.findElements(By.id(locator)).size()<1);	
		}else if (identifyBy.equalsIgnoreCase("name")){
			Assert.assertTrue(driver.findElements(By.name(locator)).size()<1);	
		}
	
	}
	
	public static void verifyTableSize(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		
		int count = Integer.parseInt(valuetoVerify);
		if (identifyBy.equalsIgnoreCase("xpath")){
		    //RemoteWebDriver rmtDriver = (RemoteWebDriver) driver;		
			List<WebElement> rows = driver.findElements(By.xpath(locator));
			int records = rows.size(); 
			Assert.assertEquals(count, records);
		}else if (identifyBy.equalsIgnoreCase("name")){
			List<WebElement> rows = driver.findElements(By.name(locator));
			int records = rows.size(); 
			Assert.assertEquals(count, records);
		}else if(identifyBy.equalsIgnoreCase("class")){
			List<WebElement> rows = driver.findElements(By.className(locator));
			int records = rows.size(); 
			Assert.assertEquals(count, records);
		}					
	}
	
	public static void verifyWebPageTitle(WebDriver driver, ArrayList<Object>  paramList) throws Exception {
		String valuetoVerify = (String) paramList.get(SPECIAL_VALUE_FIELD);		
		
		Set<String> CtxtHndl = ((IOSDriver)driver).getContextHandles();
		CtxtHndl.size();
		Iterator iterator = CtxtHndl.iterator();
		
		
		System.out.println(CtxtHndl.size());
		
		String context1="NATIVE_APP";
		//String context2="WEBVIEW_2";
		//for(String Ctxt: CtxtHndl){
			
			if(iterator.hasNext()){
				String NatApp = iterator.next().toString();
				System.out.println(NatApp);
				if(iterator.hasNext()){
					String WBV1 = iterator.next().toString();
					System.out.println(WBV1);
					
					if(iterator.hasNext()){
						//iterator.next();
						String ContextWV;
							if(iterator.hasNext()){						
							ContextWV = iterator.next().toString();
							System.out.println(ContextWV);
							}else{
								ContextWV = iterator.toString();
								System.out.println(ContextWV);								
							}
							String PgTtl=((IOSDriver)driver).context(ContextWV).getTitle();
							Boolean result =((IOSDriver)driver).context(ContextWV).getTitle().equalsIgnoreCase(valuetoVerify);				
							((IOSDriver)driver).context(NatApp);		
							driver.findElement(By.name("Done")).click();					
									if(result == false){
										throw new Exception("************************************Text doesn't match**************************************");		            
								}
						}else{
						Boolean result =((IOSDriver)driver).context(WBV1).getTitle().equalsIgnoreCase(valuetoVerify);
						String PgTitle = ((IOSDriver)driver).context(WBV1).getTitle();
						((IOSDriver)driver).context(NatApp);
						driver.findElement(By.name("Done")).click();
						if(result == false){
							throw new Exception("************************************Text doesn't match**************************************");		            
						}
						
					}
			}
	}
		
			
			/*if(Ctxt.equalsIgnoreCase(context2)){
				WebDriver PgTitle= ((IOSDriver)driver).context(context2);
				String Title = PgTitle.getTitle();
				Boolean result =Title.equalsIgnoreCase(valuetoVerify);			
				
				((IOSDriver)driver).context(context1);		
				driver.findElement(By.name("Done")).click();
				
				if(result == false){
		            throw new Exception("************************************Text doesn't match**************************************");		            
		        }
			}*/
	}		

	
	public static void selectRadiobutton(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}

	}

	public static void selectCheckbox(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String checkFlag = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.xpath(locator)).isSelected())){
					driver.findElement(By.xpath(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("id")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.id(locator)).isSelected())){
					driver.findElement(By.id(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("name")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.name(locator)).isSelected())){
					driver.findElement(By.name(locator)).click();
				}
			}
		}
	}

	public static void selectValue(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valToBeSelected = (String) paramList.get(VALUE);
		WebElement option = null;
		
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			option = driver.findElement(By.xpath(locator));
		}else if (identifyBy.equalsIgnoreCase("id")){
			option = driver.findElement(By.id(locator));
		}else if (identifyBy.equalsIgnoreCase("name")){
			option = driver.findElement(By.name(locator));
		}else if (identifyBy.equalsIgnoreCase("class")){
			option = driver.findElement(By.className(locator));
		}
		
		//option.selectByVisibleText(valToBeSelected);
		
		//option.sendKeys(valToBeSelected);
		
				
       List <WebElement> options = driver.findElements(By.tagName("option"));
		for (WebElement option1 : options) {
			if (valToBeSelected.equalsIgnoreCase(option1.getText())){
				option1.click();
			}
		}
	}
				
	

	public static void enterKeyPress(WebDriver driver, ArrayList <Object>  paramList) throws AWTException, InterruptedException{
		
		
		Robot robot = new Robot();
		/*robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);*/
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
	}
	
	public static void scrollToEle(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
		
		/*
		 * This method uses both Locator and Text to search for the element for display and if not displayed, scrolls down five times, each time searching for the element.
		*/
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String value = (String) paramList.get(VALUE);
		
		String valueName ="//*[@name='"+value+"']" ;
		String valueLabel ="//*[@label='"+value+"']";
		
		Boolean ElemPresent = null;
		WebElement Ele = null;
		try{
		if (identifyBy.equalsIgnoreCase("xpath")){
			ElemPresent = driver.findElement(By.xpath(locator)).isDisplayed();
			Ele = driver.findElement(By.xpath(locator));
			}else if (identifyBy.equalsIgnoreCase("id")){
			ElemPresent = driver.findElement(By.id(locator)).isDisplayed();
			Ele = driver.findElement(By.id(locator));
			}else if (identifyBy.equalsIgnoreCase("name")){
			ElemPresent = driver.findElement(By.name(locator)).isDisplayed();
			Ele = driver.findElement(By.name(locator));
			}
		}catch(Exception Excep)
		{
			ElemPresent=false;
		}
		//Get the size of screen.
		Dimension size;
		  size = driver.manage().window().getSize();
		  System.out.println(size);
		   
		  //Find swipe start and end point from screen's width and height.
		  //Find starty point which is at bottom side of screen.
		  int starty = (int) (size.height * 0.80);
		  //Find endy point which is at top side of screen.
		  int endy = (int) (size.height * 0.10);
		  //Find horizontal point where you wants to swipe. It is in middle of screen width.
		  int startx = size.width / 2;
		  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

		  for(int k=0;k<=5;k++){				  			  
			  
			  Boolean textPresent = driver.findElement(By.xpath(valueName)).isDisplayed();
			  Boolean labelPresent= driver.findElement(By.xpath(valueLabel)).isDisplayed();
			  if (ElemPresent == true) {
				  Ele.click();
				  break;
			  } else if (textPresent == true) {
				  Ele.click();
				  break;
			  }else if (labelPresent == true) {
				  Ele.click();
				  break;
			  }else {
				  System.out.println("Element not present, hence scrolling down");
				  // Swipe from Bottom to Top
				  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);
			  }
		 }
		
	}
	
	public static void scrollToElement(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
		String text = (String) paramList.get(SPECIAL_VALUE_FIELD);		
		String value ="//*[@name='"+text+"']" ;
		
		/*((IOSElement) driver).scrollTo(value);
		Thread.sleep(2000);*/
		 
		 //WebElement ScrollElement = driver.findElement(MobileBy.IosUIAutomation("new UiScrollable(new UiSelector()).scrollIntoView("+"new UiSelector().text(\"Personal Details\"\"));"));
		 //ScrollElement.getLocation();
		
		//Get the size of screen.
		Dimension size;
		  size = driver.manage().window().getSize();
		  System.out.println(size);
		   
		  //Find swipe start and end point from screen's width and height.
		  //Find starty point which is at bottom side of screen.
		  int starty = (int) (size.height * 0.90);
		  //Find endy point which is at top side of screen.
		  int endy = (int) (size.height * 0.10);
		  //Find horizontal point where you wants to swipe. It is in middle of screen width.
		  int startx = size.width / 2;
		  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

		  for(int ij=0;ij<=3;ij++){		  
		  Boolean textPresent = driver.findElement(By.xpath(value)).isDisplayed();
		  if(textPresent==true){
			  //driver.findElement(By.xpath(value)).click();
			  break;
		  }else{
			  System.out.println("Element not present, hence scrolling down");
			  //Swipe from Bottom to Top
			  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 3000);
			 
		  }		  
		 }
	}
	
	public static void SwipeDown(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
				
		
		Dimension size;
		  size = driver.manage().window().getSize();
		  System.out.println(size);
		   
		  //Find swipe start and end point from screen's width and height.
		  //Find starty point which is at bottom side of screen.
		  int starty = (int) (size.height * 0.80);
		  //Find endy point which is at top side of screen.
		  int endy = (int) (size.height * 0.20);
		  //Find horizontal point where you wants to swipe. It is in middle of screen width.
		  int startx = size.width / 2;
		  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

		  //Swipe from Bottom to Top.
		  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 3000);
		  
		  //Swipe Again
		  //((AppiumDriver) driver).swipe(startx, starty, startx, endy, 3000);
		  Thread.sleep(2000);
		  //Swipe from Top to Bottom.
		 /* ((AppiumDriver) driver).swipe(startx, endy, startx, starty, 3000);
		  Thread.sleep(2000);*/
	}
	
	
	// Swipe up and down -> Was not able to make it work
	
	/*public static void SwipeDown2(WebDriver driver, ArrayList<Object>  paramList) {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "down");
		js.executeScript("mobile: scrollTo", scrollObject);
	
		}
	
	public static void SwipeUp(WebDriver driver, ArrayList<Object>  paramList) {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "up");
		js.executeScript("mobile: scrollTo", scrollObject);
	
		}*/
	
	public static void scrollToText(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
		
		String text = (String) paramList.get(SPECIAL_VALUE_FIELD);		
		String value ="//*[@name='"+text+"']";		
		
		//Get the size of screen.
		Dimension size;
		  size = driver.manage().window().getSize();
		  System.out.println(size);
		   
		  //Find swipe start and end point from screen's width and height.
		  //Find starty point which is at bottom side of screen.
		  int starty = (int) (size.height * 0.90);
		  //Find endy point which is at top side of screen.
		  int endy = (int) (size.height * 0.10);
		  //Find horizontal point where you wants to swipe. It is in middle of screen width.
		  int startx = size.width / 2;
		  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

		  for(int ij=0;ij<=5;ij++){		  
		  Boolean textPresent = driver.findElement(By.xpath(value)).isDisplayed();
		  if(textPresent==true){
			  driver.findElement(By.xpath(value)).click();
			  break;
		  }else{
			  System.out.println("Element not present, hence scrolling down");			  
			  //Swipe from Bottom to Top
			  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 1000);			 
		  }		  
		}
				
	}
	
		public static void GoBack(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
		//String value = (String) paramList.get(SPECIAL_VALUE_FIELD);
		
		//driver.close();

	}
	
		public static void TapMob(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		String value1 =  (String) paramList.get(SPECIAL_VALUE_FIELD);	
			
		String[] StrArr=value1.split(",");		
		String a1 = null;
		String a2 = null;		
		Double Ad = null;
		Double Bd = null;
		
		a1= StrArr[0];
		a2=StrArr[1];				
				
		Ad = Double.parseDouble(a1);
		Bd = Double.parseDouble(a2);	
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    HashMap<String, Double> tapObject = new HashMap<String, Double>();
	    tapObject.put("x",  Ad); // in pixels from left
	    tapObject.put("y",  Bd); // in pixels from top        
				
		js.executeScript("mobile: tap", tapObject);
	    Thread.sleep(2000);
	}

	
		public static void SwipeLeft(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		Dimension size = driver.manage().window().getSize(); 	                 
				int startx = (int) (size.width * 0.70);
				int endx = (int) (size.width * 0.20);
				int starty = size.height / 3; 
			    ((IOSDriver) driver).swipe(startx, starty, endx, starty, 1000);
			    }
		
		
		public static void SwipeRight(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		Dimension size = driver.manage().window().getSize(); 	                 
			     int startx = (int) (size.width * 0.1);
			     int endx = (int) (size.width * 0.9);
			     int starty = size.height / 2; 
			     ((IOSDriver) driver).swipe(startx, starty, endx, starty, 2000);
			     }
		

		
		
		public static void verifyLocElement(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
			String identifyBy = (String) paramList.get(IDENTIFYBY);
			String locator = (String) paramList.get(LOCATOR);
			String valueToBeVerified = (String) paramList.get(VALUE);
			
			if (identifyBy.equalsIgnoreCase("xpath")){
				String LocTxt = driver.findElement(By.xpath(locator)).getAttribute("name");
				Boolean res = LocTxt.contains(valueToBeVerified);
				if (res==false){
					throw new Exception("Didn't find the Location");
				}
				
			}else if (identifyBy.equalsIgnoreCase("id")){
				driver.findElement(By.id(locator)).isDisplayed();
			}else if (identifyBy.equalsIgnoreCase("name")){
				driver.findElement(By.name(locator)).isDisplayed();
			}
		}
		
		public static void TapToSelLibr(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
			String value1 =  (String) paramList.get(SPECIAL_VALUE_FIELD);	
				
			String[] StrArr=value1.split(",");		
			String a1 = null;
			String a2 = null;		
			Double Ad = null;
			Double Bd = null;
			
			a1= StrArr[0];
			a2=StrArr[1];				
					
			Ad = Double.parseDouble(a1);
			Bd = Double.parseDouble(a2);	
			
			JavascriptExecutor js = (JavascriptExecutor) driver;
		    HashMap<String, Double> tapObject = new HashMap<String, Double>();
		    tapObject.put("x",  Ad); // in pixels from left
		    tapObject.put("y",  Bd); // in pixels from top        
					
			js.executeScript("mobile: tap", tapObject);
		    Thread.sleep(2000);
		}
		
		public static void AcceptAllSubscriptions(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
			//String value1 =  (String) paramList.get(SPECIAL_VALUE_FIELD);	
			
			Boolean i=true;
			try{
			while(i==true){									
					WebDriverWait wait = new WebDriverWait(driver, 20);
				    wait.until(ExpectedConditions.alertIsPresent());					
					driver.switchTo().alert().accept();
				}	
				}catch(Exception E){
					//i=false;
					System.out.println("No Alert present");					
				}				
			}
		
		public static void ListOrTabView(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
			String identifyBy = (String) paramList.get(IDENTIFYBY);
			String locator = (String) paramList.get(LOCATOR);
			String value = (String) paramList.get(VALUE);		
			
			WebElement Ftr = null;
						
			String[] locatorVal = locator.split(",");			
			String locator1=locatorVal[0];
			String locator2=locatorVal[1];
			String locator3=locatorVal[2];			
			
			Dimension size;
			  size = driver.manage().window().getSize();
			  System.out.println(size);
			   
			  //Find swipe start and end point from screen's width and height.
			  //Find starty point which is at bottom side of screen.
			  int starty = (int) (size.height * 0.80);
			  //Find endy point which is at top side of screen.
			  int endy = (int) (size.height * 0.10);
			  //Find horizontal point where you wants to swipe. It is in middle of screen width.
			  int startx = size.width / 2;
			  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);			
			
			//Verify if the app is displayed in TAB mode
			try{
				driver.findElement(By.xpath("//XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeTabBar")).isEnabled();
				System.out.println("Tab Mode...");					
			
				// Verify if the element is available as one of the TAB options
				try{		
					//System.out.println(driver.getPageSource());					
					Ftr =driver.findElement(By.xpath(locator1));							
					System.out.println("Available to click...");					
					Ftr.click();				
					}
				// Since the element is not available as a tab option, click the More tab and scroll to the feature.
				catch(NoSuchElementException NSE){					
						driver.findElement(By.xpath("//*[@name='More']")).click();
						try{
							//System.out.println(driver.getPageSource());		
							Ftr = driver.findElement(By.xpath(locator2));
							
							for(int k=0;k<3;k++){													
								if(Ftr.getLocation().getX()==0&Ftr.getLocation().getY()==0){
									System.out.println("Element not in view, hence scrolling down");
									((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);
									break;
									}else if(Ftr.getLocation().getY()>driver.manage().window().getSize().getHeight()-50){
										((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);
										break;
										}else{
											break;
										}							  						
									}
							Ftr.click();
							
							/*if (Ftr.isEnabled() == true){
								System.out.println("Location before scrolling - "+Ftr.getLocation());								
								System.out.println(driver.findElement(By.xpath("//*[@name='My Card']")).getLocation());
								System.out.println("Screen sixe is - "+ driver.manage().window().getSize());
								System.out.println("*********************");
								
								if(Ftr.getLocation().getX()==0&Ftr.getLocation().getY()==0){
									((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);
								}
								//((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);	
								System.out.println("Location after scrolling - "+Ftr.getLocation());								
								System.out.println(driver.findElement(By.xpath("//*[@name='My Card']")).getLocation());
								System.out.println("Screen sixe is - "+ driver.manage().window().getSize());
								Ftr.click();
							}else{
								 System.out.println("Element not present, hence scrolling down");
								  // Swipe from Bottom to Top
								  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);	
								  Ftr.click();
								}*/
							}catch(NoSuchElementException Exx){
								/*// Didn't find hence scrolling down
								Ftr = driver.findElement(By.xpath(locator2));
									  for(int k=0;k<3;k++){											  
									  if (Ftr.isDisplayed() == true) {										  
										  Ftr.click();
										  break;
									  } else {
										  System.out.println("Element not present, hence scrolling down");
										  // Swipe from Bottom to Top
										  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);
									  }									  
								 }
									  Ftr.click();
								//((ScrollsTo)driver).scrollTo(value);
								       WebElement Ftr = driver.findElement(By.xpath(locator2));
								JavascriptExecutor executor = (JavascriptExecutor)driver;
								executor.executeScript("arguments[0].scrollIntoView(true);", Ftr);					
								Thread.sleep(3000);
								//if(Ftr.isDisplayed())
									Ftr.click();   */  
								System.out.println("Element is not present on this page ...");
							}
					}
				}
			//App is displayed in List mode hence scroll to feature directly.
			catch(NoSuchElementException Ex){
				System.out.println(driver.getPageSource());		
				Ftr = driver.findElement(By.xpath(locator3));
					System.out.println("Should be List Mode.........");
					try{
						for(int k=0;k<3;k++){													
							if(Ftr.getLocation().getX()==0&Ftr.getLocation().getY()==0){
								System.out.println("Element not in view, hence scrolling down");
								((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);
								break;
								}else if(Ftr.getLocation().getY()>driver.manage().window().getSize().getHeight()-50){
									((AppiumDriver) driver).swipe(startx, starty, startx, endy, 2000);
									break;
									}else{
										break;
									}							  						
								}
						Ftr.click();
					}catch(Exception Exp){
						
						System.out.println("Element is not present on this page ...");
								
						}
					}
				
			}
		
		
		
}		
		
		
	
	








