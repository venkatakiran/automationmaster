package com.tss.hybridFrameWork;

import io.appium.java_client.AppiumDriver;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.MalformedURLException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.MissingResourceException;

import jxl.read.biff.BiffException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.WebDriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.tss.dataReader.XLSReader;
import com.tss.driver.DriverFactory;
import com.tss.testReport.Report;
import com.tss.utils.PropertiesReader;
import com.tss.utils.StartStopAppium;
import com.tss.utils.TestUtils;

public class DriverApp {
  String coreFileKey = "CoreFileName" ;
  String dataFileKey = "DataFileName" ;
  String strSuiteSheetName= "Suite";
  String strCoreFileName ;
  String strDataFileName ;
  PropertiesReader applicationPrty;
  PropertiesReader objectPrty;
  private WebDriver driver;	
  final String TCID = "TCID" ;
  final String DESC = "Description" ;
  final String TSID = "TSID";
  final String KEYWORD = "Keyword";
  final String OBJECT = "Object";
  final String DATA_CLM_NAME =	"DataClmnName";
  final String DATA_CLM_NAME2 =	"DataClmnName2";		
  final String DATA_CLM_NAME3 =	"DataClmnName3";		
  final String DATA_CLM_NAME4 =	"DataClmnName4";		
  final String DATA_CLM_NAME5 =	"DataClmnName5";
  final String PROCEED_ONFAIL = "ProceedOnFail";
  final String RUNMODE = "RunMode";
  String strReprotPath = null;
  String strDateFormat = "dd,MMMM,yyyy hh:mm:ss";
  
  private static final Logger log = LoggerFactory.getLogger(DriverApp.class) ;
  private Report rpt;
  
  Process appium;
  
  private void loadObjectProperty(String strObjPrefix){
	  
	  String objClassPath = "com.tss.objectproperties.";
	  
	  try {
		  	objectPrty = new PropertiesReader(objClassPath + strObjPrefix.toLowerCase() + "object");
	  }
	  catch (MissingResourceException e) {
		    objectPrty = new PropertiesReader(objClassPath +"object");
			System.out.println(e.getMessage());
	  }
	
  }
    
  @BeforeClass
  public void initialize() throws IOException, InterruptedException{
	 
	  // ***************Starting the Appium server*****************.
	   //StartStopAppium.startAppium();
	   //Thread.sleep(5000);
	   
	  String [] drvProperties = null;
	  
	try {
					
		  	ArrayList <Object> param = new ArrayList<Object>(); 
		  	applicationPrty = new PropertiesReader("config.appconfig");
			String strDriverType = applicationPrty.getProperty("DriverType");
			loadObjectProperty(strDriverType);
			driver = DriverFactory.getDriverInstance(strDriverType + "Driver");
			rpt = new Report();			
			
		}
		catch (MissingResourceException e) {
			System.out.println(e.getMessage());
			
		}
	  
  }
  
  
  @Test
  public void TestApplication() {
	  int totalTestCase = 0;
	  int totalTestCaseSkipped = 0;
	  int totalTestCasePassed = 0;
	  String strStartTime = null;
	  String strEndTime = null;
	  String strResult = null;
	  
	  ArrayList <Object> paramList = new ArrayList<Object>();
	 	  
	  log.info("started");
	  XLSReader xlsTestCaseReader = new XLSReader();
	  XLSReader xlsDataReader = new XLSReader();
  
  
		strCoreFileName = applicationPrty.getProperty(coreFileKey);
		strDataFileName = applicationPrty.getProperty(dataFileKey);
		strReprotPath = applicationPrty.getProperty("ReportPath");
		log.info("Core File Name to be executed: " + strCoreFileName);
		log.info("Data File Name to fetch the data: " + strDataFileName);
			
	  try {
		  	rpt.startReport(strReprotPath + strSuiteSheetName+ ".xml");
		  	rpt.startTestSuite(strSuiteSheetName);
		  	xlsTestCaseReader.openWorkBook(strCoreFileName);
			xlsDataReader.openWorkBook(strDataFileName);
			xlsTestCaseReader.setActiveSheet(strSuiteSheetName);
			log.info("Started Suite Execution : " + strSuiteSheetName);
						
			totalTestCase = xlsTestCaseReader.getRowCount();
			log.info("Total number of test cases : " + totalTestCase);
			
			for (int testCase = 1; testCase < totalTestCase; testCase++){
				strStartTime = TestUtils.dateTimeNow(strDateFormat);
				Hashtable <String, String> testCaseData = xlsTestCaseReader.readRow((testCase));
				
				rpt.startTestCase(testCaseData.get(TCID), testCaseData.get(DESC));
				if (testCaseData.get(RUNMODE).equalsIgnoreCase("Y")){
					log.info("Test Case started : " + testCaseData.get(TCID) + "  " + testCaseData.get(DESC));
					
					if (runTestSteps(xlsTestCaseReader, xlsDataReader, testCaseData.get(TCID))){
						System.out.println("Pass - Test Case : " + testCaseData.get(TCID));
						totalTestCasePassed ++;
						log.info("Pass Test Case : " + testCaseData.get(TCID) + "  " + testCaseData.get(DESC));
						strResult = "Pass";
					}
					else{
						log.info("Failed - Test Case : " + testCaseData.get(TCID) + "  " + testCaseData.get(DESC));
						strResult= "Fail";
						
						
						/*
						 * This is the clean up script after every test scenario failure. Currently, it is only till the Organization level
						 * *****************************************************************************************************************
						 */
						
						KeywordDriver.navigateToOrgHome(driver, paramList);
						
						
					}
					
					
				}
				else{
					log.info("Skipped test case : " + testCaseData.get(TCID) + "  " + testCaseData.get(DESC));
					strResult= "Skipped";
					totalTestCaseSkipped ++;
				}
				strEndTime = TestUtils.dateTimeNow(strDateFormat);
				rpt.closeTestCase(strStartTime, strEndTime , strResult);
				xlsTestCaseReader.setActiveSheet(strSuiteSheetName);
						
			}
			
		} catch (BiffException e1) {
				e1.printStackTrace();
		} catch (IOException e1) {
		
			e1.printStackTrace();
		}catch (Exception e1){
			e1.printStackTrace();
		}
	  
		
		logTestSuiteSummary((totalTestCase - 1), totalTestCasePassed, (totalTestCase - 1 - totalTestCaseSkipped- totalTestCasePassed), totalTestCaseSkipped);
		rpt.closeTestSuite(totalTestCasePassed, (totalTestCase - 1 - totalTestCaseSkipped- totalTestCasePassed), totalTestCaseSkipped, "20:20:20", "20:20:20");
	  
  }
  
  @AfterClass
  public void tearDown() throws ExecuteException, IOException, InterruptedException{
	  	  rpt.closeReport();
	  	  //driver.close();	  
	  
	//***************Stop the Appium server*****************  
	  	//StartStopAppium.stopAppium();	  
  }
 
  
  private boolean runTestSteps(XLSReader xlsTestCaseReader , XLSReader xlsDataReader , String testCaseName)
  {
	  int totalTestSteps = 0;
	  int totalIterations = 0;
	  boolean isTestPass = true;
	  String strSCRNDateFormat = "ddMMyyyy_hhmmss";
	  String strStartTime = null;
	  String strEndTime = null;
	  String strResult = null;
	  String strSCRNShotFileName = null;
	  ArrayList <Object> paramList = new ArrayList<Object>();
	
	  	xlsTestCaseReader.setActiveSheet(testCaseName);
	  	xlsDataReader.setActiveSheet(testCaseName);
		
		
		totalTestSteps = xlsTestCaseReader.getRowCount();
		totalIterations = xlsDataReader.getRowCount();
		Hashtable <String, String> testCaseTable = null;
		Hashtable <String, String> dataTable = null;
	  
		  
		  Class[] argList = {WebDriver.class, ArrayList.class};
		 	  
			  for (int dataRw = 1; dataRw < totalIterations; dataRw++){
				  dataTable = xlsDataReader.readRow((dataRw));
				  if(isEmptyDataRow(dataTable)){
					  break;
				  }
				  
				  for (int testStep = 1; testStep < totalTestSteps; testStep++){
					  strStartTime = TestUtils.dateTimeNow(strDateFormat);
					  strSCRNShotFileName = "";
					  testCaseTable = xlsTestCaseReader.readRow((testStep));
					  	log.info("Started Test case name : " + testCaseName + " Test step : " + testCaseTable.get(TSID) + " Test step Description : " + testCaseTable.get(DESC) );
						if (!testCaseTable.get(OBJECT).isEmpty()){
							String []str = objectPrty.getProperties(testCaseTable.get(OBJECT));
							for(int i =0; i < str.length; i++){
								paramList.add(str[i]);
							}
						}
						paramList.add(dataTable.get(testCaseTable.get(DATA_CLM_NAME)));
						
						// To read more than one data column
						if(xlsTestCaseReader.getColumnCount()>6){	
							paramList.add(dataTable.get(testCaseTable.get(DATA_CLM_NAME2)));	
							paramList.add(dataTable.get(testCaseTable.get(DATA_CLM_NAME3)));
							paramList.add(dataTable.get(testCaseTable.get(DATA_CLM_NAME4)));
							paramList.add(dataTable.get(testCaseTable.get(DATA_CLM_NAME5)));
						}
						try{
							Method method = KeywordDriver.class.getMethod(testCaseTable.get(KEYWORD), argList);
							method.invoke(method, driver, paramList);
							log.info("Pass Test case name : " + testCaseName + " Test step : " + testCaseTable.get(TSID) + " Test step Description : " + testCaseTable.get(DESC) );
							strResult = "Pass";
						}	
						catch (Exception e){
							  log.info(" *** Fialed Test case name*** : " + testCaseName + " Test step : " + testCaseTable.get(TSID) + " Test step Description : " + testCaseTable.get(DESC) );
							  strSCRNShotFileName = strReprotPath + "\\img\\" + testCaseTable.get(TSID)+ TestUtils.dateTimeNow(strSCRNDateFormat) + ".png" ;
							  TestUtils.captureScreennShot(driver,strSCRNShotFileName);
							  strResult = e.toString();
							  isTestPass = false;
						  }	
						
						strEndTime = TestUtils.dateTimeNow(strDateFormat);
						rpt.addTestStep(testCaseTable.get(TSID), testCaseTable.get(DESC), strStartTime, strEndTime, strSCRNShotFileName, strResult);
						if (testCaseTable.get(PROCEED_ONFAIL).equalsIgnoreCase("N") && (!isTestPass)){
							KeywordDriver.RecoveryScenario(driver, paramList);
							 return isTestPass;
						}
						paramList.clear();
						
				  }  
			  }
			    
			  
	  return isTestPass;
  }
  private boolean isEmptyDataRow(Hashtable <String, String> dataTable){
	  boolean result = true;
	  Collection <String> dataValues = dataTable.values();
	  
	  for(String value :dataValues){
		  if (!value.isEmpty()){
			  result = false;
			  break;
		  }
			  
	  }
	  return result;
  }
  private void logTestSuiteSummary( int totalTestCase, int totalTestCasePassed, int totalTestCaseFailed, int totalTestCaseSkipped){
	  	log.info("Total Test Cases : " + totalTestCase);
		log.info("Total Test Pass : " +  totalTestCasePassed);
		log.info("Total Test Cases Failed : " + totalTestCaseFailed);
		log.info("Total Test Cases Skipped : " + totalTestCaseSkipped);
		log.info("Suite Completed");
  }
  
  		
  }

