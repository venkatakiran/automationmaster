package com.tss.hybridFrameWork;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.xpath.operations.Equals;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ByClassName;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ScrollsTo;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;



import com.tss.utils.OpenDialog;

public class KeywordDriver {
	
	static final int IDENTIFYBY = 0;
	static final int SPECIAL_VALUE_FIELD=0;	
	static final int URL =0;
	static final int LOCATOR = 1;
	static final int VALUE = 2;
	static final int VALUE2 = 3;
	static final int XPATH1 = 1;
	static final int XPATH2 = 2;
	static final int XPATH3 = 3;
	
	static final int SPECIAL_VALUE_FIELD1 = 1;
	static final int SPECIAL_VALUE_FIELD2 = 2;
	static final int SPECIAL_VALUE_FIELD3 = 3;
	static final int SPECIAL_VALUE_FIELD4 = 4;
	static final int SPECIAL_VALUE_FIELD5 = 5;
			
	
	public static void GoBack(WebDriver driver, ArrayList<Object>  paramList){
		
		driver.navigate().back();	
		
		try{
			driver.findElement(By.id("android:id/button2")).click();
		}catch(Exception E){
			System.out.println("No popups found...");
		}
		
	}
	
	
	public static void AcceptPopup(WebDriver driver, ArrayList<Object>  paramList){
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.alertIsPresent());
		
		driver.switchTo().alert().accept();		
		
	}
	
	public static void AcceptPopupIfExists(WebDriver driver, ArrayList<Object>  paramList){
		
		WebDriverWait wait = new WebDriverWait(driver, 15);	
		
		try{
			//wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ProgressBar")));
			wait.until(ExpectedConditions.alertIsPresent());			
			driver.switchTo().alert().accept();		
		}catch(Exception E){
			try {	
				wait.until(ExpectedConditions.elementToBeClickable(By.id("android:id/button1")));	
				driver.findElement(By.id("android:id/button1")).click();	
				System.out.println("Accepted the popup");
				}
			catch(Exception Exc) {
				try {
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(driver.findElement(By.className("android.widget.FrameLayout"))));
					driver.switchTo().frame(driver.findElement(By.className("android.widget.FrameLayout")));
					driver.findElement(By.id("android:id/button1")).click();
					System.out.println("Accepted the popup");
				}
					catch(Exception E2) {
								System.out.println("No popups");
							}
					}				
		}
	}
	
	public static void DismissPopup(WebDriver driver, ArrayList <Object>  paramList) {
		
		driver.switchTo().alert().dismiss();
		
	}
	
	
	public static void VerifyPopupText(WebDriver driver, ArrayList <Object>  paramList){
		String valuetoCompare = (String) paramList.get(SPECIAL_VALUE_FIELD);
		
		//System.out.println(driver.switchTo().alert().getText());
		driver.switchTo().alert().getText().contains(valuetoCompare);
		
	}
	
	public static void wait(WebDriver driver, ArrayList <Object>  paramList){
		
		int waitTime = Integer.parseInt((String)paramList.get(SPECIAL_VALUE_FIELD));
		try {
			Thread.sleep(waitTime * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//driver.manage().timeouts().implicitlyWait(waitTime,TimeUnit.SECONDS);
	}
	
	public static void wait1(WebDriver driver, ArrayList <Object>  paramList){
		int waitTime = Integer.parseInt((String)paramList.get(SPECIAL_VALUE_FIELD));
				
		driver.manage().timeouts().implicitlyWait(waitTime,TimeUnit.SECONDS);
	}
	
	public static void waitExplicit(WebDriver driver,ArrayList<Object> paramList) {
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoType = (String) paramList.get(VALUE);
		
		
		if (identifyBy.equalsIgnoreCase("id")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
		} else if (identifyBy.equalsIgnoreCase("xpath")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		} else if (identifyBy.equalsIgnoreCase("class")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
		} else if (identifyBy.equalsIgnoreCase("name")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
		}
	}
	
	public static void waitForAbsence(WebDriver driver,ArrayList<Object> paramList) throws InterruptedException {
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoType = (String) paramList.get(VALUE);
		
		
		if (identifyBy.equalsIgnoreCase("id")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(locator)));
		} else if (identifyBy.equalsIgnoreCase("xpath")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
		} else if (identifyBy.equalsIgnoreCase("class")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(locator)));
		} else if (identifyBy.equalsIgnoreCase("name")) {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.name(locator)));
		}
		
		// Just a buffer time to avoid failures.
		Thread.sleep(2000);
	}

	public static void navigateto(WebDriver driver, ArrayList <Object>  paramList){
		driver.get((String)paramList.get(URL));
		driver.manage().window().maximize();
	}
	
	
	public static void clickButton(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		/*if (identifyBy.equalsIgnoreCase("xpath")){
			((RemoteWebDriver)driver).findElementByXPath(locator).click();
		}*/
		
		/*if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElementById(locator).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElementByName(locator).click();
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElementByClassName(locator).click();
		}*/
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			(driver).findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
		}
	}
	
	public static void clickIfExists(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		WebDriverWait wait = new WebDriverWait(driver, 10);	
				
		if (identifyBy.equalsIgnoreCase("xpath")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
				driver.findElement(By.xpath(locator)).click();
			}catch(Exception E){
				System.out.println("The element doesn't exist on the page.");
			}
		}else if (identifyBy.equalsIgnoreCase("id")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.id(locator)));
				driver.findElement(By.id(locator)).click();
			}catch(Exception E){
				System.out.println("The element doesn't exist on the page.");
			}
		}else if (identifyBy.equalsIgnoreCase("name")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.name(locator)));
				driver.findElement(By.name(locator)).click();
			}catch(Exception E){
				System.out.println("The element doesn't exist on the page.");
			}
		}else if (identifyBy.equalsIgnoreCase("class")){
			try{
				wait.until(ExpectedConditions.elementToBeClickable(By.className(locator)));
				driver.findElement(By.className(locator)).click();
		}catch(Exception E){
			System.out.println("The element doesn't exist on the page.");
		}
		}
	}

	public static void clickLink(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("link")){
			driver.findElement(By.linkText(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("plink")){
			driver.findElement(By.partialLinkText(locator)).click();
		}
	}
	
	
	public static void JSEClickElement(AndroidDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			WebElement element = driver.findElementByXPath(locator);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			
			//executor.executeScript("mobile: scrollTo", element);
			executor.executeScript("mobile: tap", element);
			
		}else if (identifyBy.equalsIgnoreCase("id")){
			WebElement element = driver.findElementById(locator);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
			
		}else if (identifyBy.equalsIgnoreCase("name")){
			WebElement element = driver.findElementByName(locator);
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
		}
	}
	
	
	public static void JSETypeInEditBox(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException{
		//String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		{
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript(locator, valuetoType);
			
			/*Thread.sleep(3000);
			WebElement iframeItem = driver.findElement(By.xpath("//*[@id='item-description_ifr'] | //*[@id='location-description_ifr']"));
			driver.switchTo().frame(iframeItem);
			Thread.sleep(2000);
			
			WebElement editorbody = driver.findElement(By.className("mce-content-body"));			
			executor.executeScript("arguments[0].innerHTML = '<p>This is a sample text created by Kiran.<p>'", editorbody);
			
			driver.findElement(By.cssSelector("body")).sendKeys("your data");
			
			driver.switchTo().defaultContent();
			Thread.sleep(2000);*/
		}
	}
	
	
	public static void EnterText(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
								
		if (identifyBy.equalsIgnoreCase("xpath")){	
			MobileElement Elem = (MobileElement) driver.findElement(By.xpath(locator));
			Elem.clear();
			Elem.sendKeys(Keys.BACK_SPACE);
			Elem.sendKeys(valuetoType);			
		}else if (identifyBy.equalsIgnoreCase("name")){
			MobileElement Elem = (MobileElement) driver.findElement(By.name(locator));
			Elem.clear();
			Elem.sendKeys(Keys.BACK_SPACE);
			Elem.sendKeys(valuetoType);		
		}else if (identifyBy.equalsIgnoreCase("class")){
			MobileElement Elem = (MobileElement) driver.findElement(By.className(locator));
			Elem.clear();
			Elem.sendKeys(Keys.BACK_SPACE);
			Elem.sendKeys(valuetoType);	
		}else if (identifyBy.equalsIgnoreCase("id")){
			MobileElement Elem = (MobileElement) driver.findElement(By.id(locator));
			Elem.clear();
			Elem.sendKeys(Keys.BACK_SPACE);
			Elem.sendKeys(valuetoType);	
		}
	}
	
	public static void EnterTextWOClear(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
								
		if (identifyBy.equalsIgnoreCase("xpath")){	
			//driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType);			
		}else if (identifyBy.equalsIgnoreCase("name")){
			//driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType);		
		}else if (identifyBy.equalsIgnoreCase("class")){
			//driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("id")){
			//driver.findElement(By.id(locator)).clear();
			driver.findElement(By.id(locator)).sendKeys(valuetoType);
		}
	}
	
	public static void EnterTextAndHitReturn(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){	
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType+"\n");
			
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType+"\n");		
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType+"\n");
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).clear();
			driver.findElement(By.id(locator)).sendKeys(valuetoType+"\n");
		}
		
	}
	
	public static void SendKeyboardKeys(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		//String chars[] = valuetoType.split("");
		
		//for(String ch:chars){		
		if (identifyBy.equalsIgnoreCase("xpath")){	
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType, Keys.ENTER);
			/*driver.findElement(By.xpath(locator)).sendKeys(Keys.NUMPAD2);
			driver.findElement(By.xpath(locator)).sendKeys(Keys.NUMPAD0);*/
			
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType, Keys.ENTER);		
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType, Keys.ENTER);
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).clear();
			driver.findElement(By.id(locator)).sendKeys(valuetoType);
						
		}
		//}
	}
	
	
	/*public static void HitReturnKey(AndroidDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		driver.sendKeyEvent(4);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).sendKeys(Keys.RETURN);
		}
	}*/
	
	public static void isTextPresent(WebDriver driver, ArrayList<Object>  paramList) {
		String identifyBy = (String) paramList.get(SPECIAL_VALUE_FIELD);		
		//driver.findElement(By.xpath("//*[contains(.,'"+ identifyBy +"')]")).getText();
		driver.findElement(By.xpath("(//*[@text='"+ identifyBy +"'])")).getText();
		//driver.findElement(By.xpath("//text()[contains(translate(.,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz'),'"+ identifyBy +"')]")).getText();
		}
	
	public static void isWebTextPresent(WebDriver driver, ArrayList<Object>  paramList) throws Exception {
		String value = (String) paramList.get(SPECIAL_VALUE_FIELD);
		System.out.println(value);
		
		//System.out.println(driver.getPageSource());
		
		Boolean result = driver.getPageSource().contains(value);		
		
		if(result == false){
			System.out.println(driver.getPageSource());
			//driver.switchTo().defaultContent();
			throw new Exception("***********Text doesn't match*************");		
			
		}else {
			System.out.println("Success");
			//driver.switchTo().defaultContent();
		}
		
		//String Text =driver.findElement(By.xpath("(//*[@class='android.webkit.WebView']//*[@class='android.view.View'])")).getText();
		//driver.findElement(By.xpath("//*[contains(.,'"+ identifyBy +"')]")).getText();
		//driver.findElement(By.xpath("(//*[@text='"+ identifyBy +"'])")).getText();
				
		//Thread.sleep(3000);	
		
		//Trial code.......................		
		/*List<WebElement> WbVw = driver.findElements(By.className("android.widget.FrameLayout"));
		int frames=WbVw.size();*/
		/*Iterator itr = WbVw.listIterator();	
		if(itr.hasNext()){
			itr.next();
				if(itr.hasNext()){
				itr.next();
					if(itr.hasNext()){
					itr.next();
						driver.switchTo().frame(itr);
				
		}*/
		//Trial code.......................	
		/*WebElement WbVw = driver.findElement(By.xpath("(//*[@class='android.widget.FrameLayout'])[3]"));
		//(By.className("android.widget.FrameLayout[@index='1']"));
		driver.switchTo().frame(WbVw);*/
		/**/
		
	}
	
	public static void verifyPageTitle(WebDriver driver, ArrayList<Object>  paramList) throws Exception {
		String value = (String) paramList.get(SPECIAL_VALUE_FIELD);
		
		/*WebElement WbVw = driver.findElement(By.className("android.widget.FrameLayout[@index='1']"));
		driver.switchTo().frame(WbVw);*/
		
		boolean result =driver.getTitle().equalsIgnoreCase(value);
		if(result == false){
			System.out.println(driver.getTitle());
			throw new Exception("***********Text doesn't match*************");			
		}else if(result == true){
			System.out.println("Success");
		}
		
	}
	
	public static void verifyText(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
			
		if (identifyBy.equalsIgnoreCase("id")){	
			String txt=driver.findElement(By.id(locator)).getText();
			System.out.println(txt);
			Boolean result = driver.findElement(By.id(locator)).getText().equalsIgnoreCase(valuetoVerify);					
			if(result == false){
	            throw new Exception("************************************Text doesn't match**************************************");
	        }			
		}
				
		else if (identifyBy.equalsIgnoreCase("name")){
			String txt = driver.findElement(By.name(locator)).getText();
			System.out.println(txt);
		Boolean result = driver.findElement(By.name(locator)).getText().equalsIgnoreCase(valuetoVerify);
		if(result == false){
            throw new Exception("************************************Text doesn't match**************************************");
        }		
		}
		else if (identifyBy.equalsIgnoreCase("class")){	
			String txt = driver.findElement(By.className(locator)).getText();
			System.out.println(txt);
			Boolean result = driver.findElement(By.className(locator)).getText().equalsIgnoreCase(valuetoVerify);				
			if(result == false){
	            throw new Exception("************************************Text doesn't match**************************************");
	        }		
			}	
		
		else if (identifyBy.equalsIgnoreCase("xpath")){	
			String txt = driver.findElement(By.xpath(locator)).getText();
			System.out.println(txt);
			Boolean result = driver.findElement(By.xpath(locator)).getText().equalsIgnoreCase(valuetoVerify);				
			if(result == false){
	            throw new Exception("************************************Text doesn't match**************************************");
	        }		
			}	
	}
	
	
	public static void verifyElement(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		//String text = driver.findElement(By.id(locator)).getText();
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).isDisplayed();
		}
	}
	
	
	public static void verifyElementNotExist(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoVerify = (String) paramList.get(VALUE);		
		
		if (identifyBy.equalsIgnoreCase("xpath")){
		Assert.assertTrue(driver.findElements(By.xpath(locator)).size()<1);
		}		
		else if (identifyBy.equalsIgnoreCase("id")){
			Assert.assertTrue(driver.findElements(By.id(locator)).size()<1);	
		}else if (identifyBy.equalsIgnoreCase("name")){
			Assert.assertTrue(driver.findElements(By.name(locator)).size()<1);	
		}
	
	}
	
	public static void verifyTableSize(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);		
		int TotalRecords = 	Integer.parseInt(valuetoVerify);
		
		int ActualRows = 0;		
		    //RemoteAndroidDriver rmtDriver = (RemoteWebDriver) driver;		
			//List<WebElement> rows = ((RemoteWebDriver)driver).findElementsByXPath(locator);
		if (identifyBy.equalsIgnoreCase("xpath")){
			List<WebElement> rows = driver.findElements(By.xpath(locator));
			ActualRows=rows.size();
			Assert.assertEquals(TotalRecords, ActualRows);
		}else if (identifyBy.equalsIgnoreCase("id")){
			List<WebElement> rows1 = driver.findElements(By.id(locator));
			ActualRows=rows1.size();
			Assert.assertEquals(TotalRecords, ActualRows);
		}else if (identifyBy.equalsIgnoreCase("class")){
			List<WebElement> rows2 = driver.findElements(By.className(locator));
			ActualRows=rows2.size();
			Assert.assertEquals(TotalRecords, ActualRows);
		}
					
	}
	
	public static void selectRadiobutton(AndroidDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElementByXPath(locator).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElementById(locator).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElementByName(locator).click();
		}

	}

	public static void selectCheckbox(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String checkFlag = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.xpath(locator)).isSelected())){
					driver.findElement(By.xpath(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("id")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.id(locator)).isSelected())){
					driver.findElement(By.id(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("name")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.name(locator)).isSelected())){
					driver.findElement(By.name(locator)).click();
				}
			}
		}
	}

	public static void selectValue(AndroidDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valToBeSelected = (String) paramList.get(VALUE);
		WebElement option = null;
		
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			option = driver.findElementByXPath(locator);
		}else if (identifyBy.equalsIgnoreCase("id")){
			option = driver.findElementById(locator);
		}else if (identifyBy.equalsIgnoreCase("name")){
			option = driver.findElementByName(locator);
		}else if (identifyBy.equalsIgnoreCase("class")){
			option = driver.findElementByClassName(locator);
		}
		
		//option.selectByVisibleText(valToBeSelected);
		
		//option.sendKeys(valToBeSelected);
		
				
       List <WebElement> options = driver.findElementsByTagName("option");
		for (WebElement option1 : options) {
			if (valToBeSelected.equalsIgnoreCase(option1.getText())){
				option1.click();
			}
		}
	}
				
	

	public static void enterKeyPress(AndroidDriver driver, ArrayList <Object>  paramList) throws AWTException, InterruptedException{
		
		
		Robot robot = new Robot();
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
	}
	
/*public static void scrollToEle(AndroidDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String value = (String) paramList.get(VALUE);
	
		HashMap<String, String> scrollObject = new HashMap<String, String>();
	//Must be an element with scrollable  property; Android (ListView,ScrollabeView) IOS (UIAScrollableView
	RemoteWebElement element = (RemoteWebElement) driver.findElement(By.tagName("ListView"));
	JavascriptExecutor js = (JavascriptExecutor) driver;
	String widId = ((RemoteWebElement) element).getId();
	 //Text for search on the screen
	scrollObject.put("text", value);
	scrollObject.put("element", widId);
	js.executeScript("mobile: scrollTo", scrollObject);
}*/


public static void scrollToElement(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
	String value = (String) paramList.get(SPECIAL_VALUE_FIELD);
	
	try{
	((ScrollsTo)driver).scrollTo(value);
	//Thread.sleep(2000);
	}catch(Exception Exc){
		try{
			// Click the cancel button on the date picker
			driver.findElement(By.id("android:id/button2")).click();
			}catch(Exception Ex){
				System.out.println("****** No date picker seen ******");
		}
	}
}

public static void scrollToFeature(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
	String identifyBy = (String) paramList.get(IDENTIFYBY);
	String locator = (String) paramList.get(LOCATOR);
	
	((ScrollsTo)driver).scrollTo(locator);
	Thread.sleep(2000);
}

public static void scrollToEle(AndroidDriver driver, ArrayList <Object>  paramList){
	String value = (String) paramList.get(SPECIAL_VALUE_FIELD);
	
	JavascriptExecutor js = (JavascriptExecutor) driver;
	js.executeScript("mobile: scrollTo", value);	
}

public static void SetDOB(WebDriver driver, ArrayList <Object>  paramList){
	String identifyBy = (String) paramList.get(IDENTIFYBY);
	String locator = (String) paramList.get(LOCATOR);
		
	if (identifyBy.equalsIgnoreCase("xpath")){		
		List<WebElement> DtPkr = driver.findElements(By.xpath(locator));               
        DtPkr.get(0).sendKeys("10");
        DtPkr.get(1).sendKeys("Oct");
        DtPkr.get(2).sendKeys("2000");
        
	}else if (identifyBy.equalsIgnoreCase("id")){
		List<WebElement> DtPkr = driver.findElements(By.id(locator));            
		DtPkr.get(0).sendKeys("10");
        DtPkr.get(1).sendKeys("Oct");
        DtPkr.get(2).sendKeys("2000");
        
	}else if (identifyBy.equalsIgnoreCase("name")){
		List<WebElement> DtPkr = driver.findElements(By.name(locator));      
		DtPkr.get(0).sendKeys("10");
        DtPkr.get(1).sendKeys("Oct");
        DtPkr.get(2).sendKeys("2000");
        
	}else if (identifyBy.equalsIgnoreCase("class")){
		List<WebElement> DtPkr = driver.findElements(By.className(locator));                
		DtPkr.get(0).sendKeys("10");
        DtPkr.get(1).sendKeys("Oct");
        DtPkr.get(2).sendKeys("2000");
	}
	
}	

	public static void TapMobCoo(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		JavascriptExecutor js = (JavascriptExecutor) driver;
		
		double val1 = 0.30;
		double val2 = 0.20;
	    HashMap<String, Double> tapObject = new HashMap<String, Double>();
	   /* tapObject.put("x", val1); // in pixels from left
	    tapObject.put("y", val2); // in pixels from top  
	    Thread.sleep(1000);*/
	    outerloop:
	    for(int i=1;i<=10;i++){	  	    	
	    	val1=val1*i;	    	
	    	for(int j=1;j<=10;j++){	    		
		    	try{
		    		WebElement cancelBtn = driver.findElement(By.id("com.sec.android.gallery3d:id/cancel_button"));
				    if (cancelBtn.isDisplayed()){			    	
				    	val2=val2*j;
				    	tapObject.put("x", val1); // in pixels from left
					    tapObject.put("y", val2); // in pixels from top  
					    //Thread.sleep(1000);	 				    
					    js.executeScript("mobile: tap", tapObject);					    
				    	} 
		    		}catch(Exception Ex){
				    	break outerloop;
				    }
	    }
	    }
	    
	    System.out.println("Val1 is "+val1);
	    System.out.println("Val2 is "+val2);
	   // js.executeScript("mobile: tap", tapObject);
	    Thread.sleep(2000);
	   /* System.out.println("Tapping again");        
	    js.executeScript("mobile: tap", tapObject);*/

}
	public static void SwipeLeft(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		 Dimension size = driver.manage().window().getSize(); 	                 
	        int startx = (int) (size.width * 0.90);
	        int endx = (int) (size.width * 0.1);
	        int starty = size.height / 2; 
	        ((AppiumDriver) driver).swipe(startx, starty, endx, starty, 2000);

}
	public static void SwipeRight(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		 Dimension size = driver.manage().window().getSize(); 	                 
	        int startx = (int) (size.width * 0.3);
	        int endx = (int) (size.width * 0.9);
	        int starty = size.height / 2; 
	        ((AppiumDriver) driver).swipe(startx, starty, endx, starty, 2000);

}
	public static void SwitchFrame(WebDriver driver, ArrayList <Object>  paramList){
		
		WebElement frm = driver.findElement(By.id("android:id/content"));
		driver.switchTo().frame(frm);

}
	public static void SwitchFrameToDefault(WebDriver driver, ArrayList <Object>  paramList){
			
		driver.switchTo().frame(0);

}
	public static void TapMob(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		
		String Coords1 =  (String) paramList.get(SPECIAL_VALUE_FIELD);
		String Coords2 =  (String) paramList.get(SPECIAL_VALUE_FIELD1);	
			
		String[] StrArr=Coords1.split(";");		
		String a1 = null;
		String a2 = null;		
		Double Ad ;
		Double Bd ;
		
		a1= StrArr[0];
		a2=StrArr[1];				
				
		Ad = Double.parseDouble(a1);
		Bd = Double.parseDouble(a2);
		/*Ad=750.0;
		Bd=1150.0;*/
	
		try {
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    HashMap<String, Double> tapObject = new HashMap<String, Double>();
	    tapObject.put("x",  Ad); // in pixels from left
	    tapObject.put("y",  Bd); // in pixels from top        
				
	    System.out.println("Before Tapping"); 
		js.executeScript("mobile: tap", tapObject);
		System.out.println("After Tapping");
		//Thread.sleep(2000);
	    //System.out.println("Tapping again");        
	    //js.executeScript("mobile: tap", tapObject);	
		
		}catch(Exception E) {
			
			System.out.println("JSExecutor didn't work. Trying the Touch Actions");
			TouchAction touch = new TouchAction((MobileDriver) driver);
			
			
			String[] StrArr2=Coords2.split(";");		
			String a3 = null;
			String a4 = null;		
			int xCoo ;
			int yCoo ;
			
			a3= StrArr2[0];
			a4=StrArr2[1];				
					
			xCoo = Integer.parseInt(a3);
			yCoo = Integer.parseInt(a4);
			
			
			touch.tap(xCoo, yCoo).perform();
			
		}
	}
	
	public static void verifyLocElement(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valueToBeVerified = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			String LocTxt = driver.findElement(By.xpath(locator)).getAttribute("text");
			Boolean res = LocTxt.contains(valueToBeVerified);
			if (res==false){
				throw new Exception("Didn't find the Location");
			}
			
		}else if (identifyBy.equalsIgnoreCase("id")){
			String LocTxt = driver.findElement(By.id(locator)).getAttribute("text");
			Boolean res = LocTxt.contains(valueToBeVerified);
			if (res==false){
				throw new Exception("Didn't find the Location");
			}
			//driver.findElement(By.id(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("name")){
			String LocTxt = driver.findElement(By.name(locator)).getAttribute("text");
			Boolean res = LocTxt.contains(valueToBeVerified);
			if (res==false){
				throw new Exception("Didn't find the Location");
			}
			//driver.findElement(By.name(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("class")){
			String LocTxt = driver.findElement(By.className(locator)).getAttribute("text");
			Boolean res = LocTxt.contains(valueToBeVerified);
			if (res==false){
				throw new Exception("Didn't find the Location");
			}
		}
	}

	/*public static void ClickItem(WebDriver driver, ArrayList <Object>  paramList){
		
		try{
		driver.findElement(By.id("com.beeonics.android.gadgetlabs:id/itemContainer1")).click();
		}catch(Exception E){
			System.out.println("Trying 2");
			try{
			driver.findElement(By.xpath("//android.widget.LinearLayout[1]")).click();
			}catch(Exception E2){
				System.out.println("Trying 3");
				try{
					driver.findElement(By.xpath("//android.widget.LinearLayout[@index='1']")).click();
				}catch(Exception E3){
					System.out.println("Trying 4");
					try{
						driver.findElement(By.xpath("//android.widget.ListView/android.widget.LinearLayout[@index='1']")).click();
					}catch(Exception E4){
						System.out.println(E4);
					}
				}
		//	}
		//}
		
	}*/
	
	
	public static void GetXMLOfPage(WebDriver driver, ArrayList <Object>  paramList){
		System.out.println(driver.getPageSource());
	}
	
	
	public static void ListOrTabView(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String value = (String) paramList.get(VALUE);		
		
		Thread.sleep(5000);
		//Verify if the app is displayed in TAB mode
		try{
			driver.findElement(By.id("android:id/tabs")).isEnabled();
			System.out.println("Tab Mode...");				
		
			// Verify if the element is available as one of the TAB options
			try{					
				/*String xp1 = "//*[@class='tab-bar tabs-icon-top']";	
				String xp2= xp1+locator;*/
				WebElement Ftr =driver.findElement(By.xpath(locator));
				Thread.sleep(3000);				
				System.out.println("Available to click...");
				//System.out.println(driver.getPageSource());
				Ftr.click();				
				}
			// Since the element is not available as a tab option, click the More tab and scroll to the feature.
			catch(NoSuchElementException NSE){					
					driver.findElement(By.xpath("//*[@text='More']")).click();
					try{
						WebElement Ftr = driver.findElement(By.xpath(locator));
						Ftr.click();
					}catch(Exception Exx){
						((ScrollsTo)driver).scrollTo(value);
						WebElement Ftr = driver.findElement(By.xpath(locator));
						/*JavascriptExecutor executor = (JavascriptExecutor)driver;
						executor.executeScript("arguments[0].scrollIntoView(true);", Ftr);*/					
						Thread.sleep(3000);
						Ftr.click();
					}
				}
			}
		//App is displayed in List mode hence scroll to feature directly.
		catch(Exception Ex){
				Thread.sleep(3000);
				System.out.println("List Mode.........");
				try{
					WebElement Ftr = driver.findElement(By.xpath(locator));
					Ftr.click();
				}catch(Exception Exp){
					((ScrollsTo)driver).scrollTo(value);
					WebElement Ftr = driver.findElement(By.xpath(locator));
					/*JavascriptExecutor executor = (JavascriptExecutor)driver;
					executor.executeScript("arguments[0].scrollIntoView(true);", Ftr);*/
					//((ScrollsTo)driver).scrollTo(value);
					Thread.sleep(2000);
					Ftr.click();
							
					}
				}
			
		}
		
	
	public static void RecoveryScenario(WebDriver driver, ArrayList <Object>  paramList){	
		
		System.out.println("Triggerred the recovery scenario.");
		
		// Navigate back to home screen only when logged in to the app.
		try{
		driver.findElement(By.id("android:id/action_bar")).isDisplayed();
		
			while(!(driver.findElements(By.id("android:id/button2")).size()==1)){
				driver.navigate().back();	
				System.out.println("Backed");
					try{
						driver.findElement(By.id("android:id/button2")).click();
						break;
						}catch(Exception E){
						System.out.println("No popups found...");
						}
			}
		}catch(Exception Ex){
			Ex.getMessage();
		}
		
	}
	
	public static void CalendarNavigation(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		/*String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);*/
		
		String ExpMAndY = (String) paramList.get(SPECIAL_VALUE_FIELD);		
		
			WebDriverWait wait = new WebDriverWait(driver, 40);			
			int i=0;
			for(;i<=6;i++){
				String ActMAndY = driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/title")).getText().trim();			
				if(!ExpMAndY.equalsIgnoreCase(ActMAndY)){
					System.out.println("Not the month I am looking for...7");
					}else{					
						System.out.println(">>>Found the month>>>");
						break;
					}
				driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/next")).click();
				if(i==6){
					i=100;
					driver.navigate().back();
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ProgressBar")));
					driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/calender_calendarMenu")).click();
					for(;i<=106;i++){
						String ActMAndY1 = driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/title")).getText().trim();			
						if(!ExpMAndY.equalsIgnoreCase(ActMAndY1)){
							System.out.println("Not the month I am looking for...");
							}else{					
								System.out.println(">>>Found the month>>>");
								break;
							}
						driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/previous")).click();
					}
				}			
			}
		}



	public static void contentSldrNav(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
			
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		WebDriverWait wait = new WebDriverWait(driver, 60);		
		
		String SiteTitle1 = "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more";
		String SiteTitle2 = "Yahoo";
		String SiteTitle3 = "W3Schools Online Web Tutorials";		
		
		for(int i=1;i<=3;i++){
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ProgressBar")));
		
		// Click the first image on the slider
		driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/image_home")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("com.gadgetsoftware.android.gadgetlabs:id/titleView")));
		
		
		String ActualText=driver.findElement(By.id(locator)).getText();
		System.out.println(ActualText);
		/*try{			  
			    switch(i){  
			    case 1: try{
			    SiteTitle1.equalsIgnoreCase(ActualText);
			    }catch(Exception Exc){System.out.println("Title doesn't match dude!");
				driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/doneButton")).click();
			    };
			    break;  
			    case 2: try{
				    SiteTitle2.equalsIgnoreCase(ActualText);
				    }catch(Exception Exc){System.out.println("Title doesn't match dude!");
					driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/doneButton")).click();
				    };
				    break;  
			    case 3: try{
				    SiteTitle3.equalsIgnoreCase(ActualText);
				    }catch(Exception Exc){System.out.println("Title doesn't match dude!");
					driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/doneButton")).click();
				    };
				    break;  
			    default:System.out.println("Doesn't fall under any of the cases");  
			    }
			driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/doneButton")).click();
		}catch(Exception Ex){
			System.out.println("dude!!! What did u catch?");
			driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/doneButton")).click();
		}*/
		
		try{
			if(ActualText.equalsIgnoreCase(SiteTitle1)|ActualText.equalsIgnoreCase(SiteTitle2)|ActualText.equalsIgnoreCase(SiteTitle3)){				
				System.out.println("Title matched");
			}
			driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/doneButton")).click();
		}catch(Exception Excep){
			driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/doneButton")).click();
			System.out.println("No match found");
		}
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ProgressBar")));
		Thread.sleep((i+i)*1000);
		}		
	}
	
	
	public static void ScrollToElemAndClickJS(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException{		
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);

		if (identifyBy.equalsIgnoreCase("xpath")) {
			WebElement element = driver.findElement(By.xpath(locator));
			
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].scrollIntoView(true);", element);
				element.click();
				Thread.sleep(2000);
			
		} else if (identifyBy.equalsIgnoreCase("id")) {
			WebElement element = driver.findElement(By.id(locator));
			
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(2000);
			

		} else if (identifyBy.equalsIgnoreCase("name")) {
			WebElement element = driver.findElement(By.name(locator));
			
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].scrollIntoView(true);", element);
				Thread.sleep(2000);
			}
	}
	
	public static void SwipeDown(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
				
		
		Dimension size;
		  size = driver.manage().window().getSize();
		  System.out.println(size);
		   
		  //Find swipe start and end point from screen's width and height.
		  //Find starty point which is at bottom side of screen.
		  int starty = (int) (size.height * 0.80);
		  //Find endy point which is at top side of screen.
		  int endy = (int) (size.height * 0.20);
		  //Find horizontal point where you wants to swipe. It is in middle of screen width.
		  int startx = size.width / 2;
		  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

		  //Swipe from Bottom to Top.
		  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 3000);
		  
		  //Swipe Again
		  //((AppiumDriver) driver).swipe(startx, starty, startx, endy, 3000);
		  Thread.sleep(2000);
		  //Swipe from Top to Bottom.
		 /* ((AppiumDriver) driver).swipe(startx, endy, startx, starty, 3000);
		  Thread.sleep(2000);*/
	}
	
	public static void SwipeDownPLAndClick(WebDriver driver, ArrayList<Object>  paramList) throws Exception {
			
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String SwipeCoo = (String) paramList.get(VALUE);	
		
		/*int h1,h2;
		
		String[] Coords=SwipeCoo.split(",");		
		String HeightUp = Coords[0];
		String HeightDown= Coords[1];	
		
		System.out.println(HeightUp+"*********"+HeightDown);
		
		h1 = Integer.parseInt(HeightUp);
		h2 = Integer.parseInt(HeightDown);
		*/
		try {
		KeywordDriver.clickButton(driver, paramList);
		}catch(Exception Exc) {
			
			  Dimension size;
			  size = driver.manage().window().getSize();
			  System.out.println(size);
			   
			  //Find swipe start and end point from screen's width and height.
			  //Find starty point which is at bottom side of screen.
			  int starty = (int) (size.height * 0.5);
			  //Find endy point which is at top side of screen.
			  int endy = (int) (size.height * 0.2);
			  //Find horizontal point where you wants to swipe. It is in middle of screen width.
			  int startx = size.width / 2;
			  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);
	
			  //Swipe from Bottom to Top.
			  ((AppiumDriver) driver).swipe(startx, starty, startx, endy, 3000);
			 
			  try {
					KeywordDriver.clickButton(driver, paramList);
					}catch(Exception E) {						
						throw(E);
					}
			 
		}
	}
	
	public static void SignInToApp(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String Email = (String) paramList.get(VALUE);		
		String Pwd = (String) paramList.get(VALUE2);	
		
		WebDriverWait wait = new WebDriverWait(driver, 30);		
		
		String[] SignInCreds=locator.split(",");
		
			String EmailIdLoc = SignInCreds[0];
			String PswdLoc= SignInCreds[1];
			
		//Sign in to the app
			
			if (identifyBy.equalsIgnoreCase("id")) {
				try {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(EmailIdLoc)));
					driver.findElement(By.id(EmailIdLoc)).sendKeys(Email);
					driver.findElement(By.id(PswdLoc)).sendKeys(Pwd);
					driver.navigate().back();
					driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/btnSignInButton")).click();
					}catch(Exception Exc) {
							System.out.println("May have already signed in");
						}
			}
				
			else if (identifyBy.equalsIgnoreCase("xpath")) {
				try {
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EmailIdLoc)));
					driver.findElement(By.xpath(EmailIdLoc)).sendKeys(Email);
					driver.findElement(By.xpath(PswdLoc)).sendKeys(Pwd);
					driver.navigate().back();
					driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/btnSignInButton")).click();
					}catch(Exception Exc) {
							System.out.println("May have already signed in");
						}
			}			
	
			else if (identifyBy.equalsIgnoreCase("name")) {
					
				try {
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(EmailIdLoc)));
						driver.findElement(By.name(EmailIdLoc)).sendKeys(Email);
						driver.findElement(By.name(PswdLoc)).sendKeys(Pwd);
						driver.navigate().back();
						driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/btnSignInButton")).click();
						}catch(Exception Exc) {
								System.out.println("May have already signed in");
							}
						}
			else if (identifyBy.equalsIgnoreCase("class")) {
				
				try {
						wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(EmailIdLoc)));
						driver.findElement(By.className(EmailIdLoc)).sendKeys(Email);
						driver.findElement(By.className(PswdLoc)).sendKeys(Pwd);
						driver.navigate().back();
						driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/btnSignInButton")).click();
						}catch(Exception Exc) {
								System.out.println("May have already signed in");
							}
						}
			
	}	
	
	
	public static void ZipAFolder(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		
		try
		  {
		    File inFolder=new File("D:\\sel\\framework\\tests");
		    File outFolder=new File("D:\\sel\\framework\\results\\results.zip");
		 
		    ZipOutputStream out = new ZipOutputStream(new  BufferedOutputStream(new FileOutputStream(outFolder)));
		   BufferedInputStream in = null;
		   byte[] data  = new byte[1000];
		   String files[] = inFolder.list();
		 
		  for (int i=0; i<files.length; i++)
		  {
		     in = new BufferedInputStream(new FileInputStream(inFolder.getPath() + "/" + files[i]), 1000);  
		     out.putNextEntry(new ZipEntry(files[i])); 
		     int count;
		              while((count = in.read(data,0,1000)) != -1)
		               {
		                    out.write(data, 0, count);
		               }
		              out.closeEntry();
		  }
		 out.flush();
		 out.close();
		  }
		 catch(Exception e)
		 {
		  e.printStackTrace();
		  }
		
	}
	
	public static void SwitchToWebView(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
	
		String f = null;
		Thread.sleep(15000);
		
		
	// Switching to webview
	Set<String> contexts = ((AppiumDriver) driver).getContextHandles();
	
	for (Iterator<String> it = contexts.iterator(); it.hasNext(); ) {
        f = it.next();
        if (f.equals("WEBVIEW_com.gadgetsoftware.android.gadgetlabs"))
            System.out.println("String found");
        //break;
    }
	
	//((AppiumDriver) driver).context("WEBVIEW_com.gadgetsoftware.android.gadgetlabs");
	
	
	/*for (String context : contexts) {
        System.out.println(contexts);
       if (context.equals("NATIVE_APP")) {
           ((AppiumDriver) driver).context((String) contexts.toArray()[1]);
           break;
       }
   }*/
//		do web testing
	driver.findElement(By.xpath("//*[@class='android.webkit.WebView']//*[@text='Link to a feature - Locations']")).click();

	//driver.findElement(By.xpath("//a[contains(.,'Link to a feature')]")).click();

	
	//driver.findElement(By.)
	
//		Switching back to NATIVE_APP
	((AppiumDriver) driver).context("NATIVE_APP");	
	}
	
	
	
	public static void VerifyCountOfItems(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String ItemCount = (String) paramList.get(VALUE);	
		
		int PLItems = Integer.valueOf(ItemCount);
		
		ArrayList<String> al= new ArrayList<String>();
		ArrayList<String> al2= new ArrayList<String>();
		ArrayList <String> al4= new ArrayList<String>();
		ArrayList <String> al5 = new ArrayList<String>();
		
		
		int Total = 0;			
		int newTot = 0;
		
		List<WebElement> items =  driver.findElements(By.id(locator));
		System.out.println("Items count on first screen before scrolling down = "+items.size());
		
		int FirstScrElems = items.size();
		
		
		for(int i=0;i<=FirstScrElems-1;i++) {			
			String name = items.get(i).getText();	
			System.out.print(name+", ");
			al.add(name);
		}
		
		System.out.println("\n"+"----------------------------------------");		
		
		for(int j=0;j<=5;j++) {		
		
		KeywordDriver.SwipeDown(driver, paramList);
		Thread.sleep(3000);			
			
		List<WebElement> items2 =  driver.findElements(By.id(locator));
		System.out.println("Items count on second screen after scrolling down = "+items2.size());	
		
		int NextScrElems = items2.size();		
			
		for(int i=0;i<=NextScrElems-1;i++) {			
				String name = items.get(i).getText();	
				System.out.print(name+", ");
				al2.add(name);
		}
		
		System.out.println("\n"+"----------------------------------------");
		
		
		
		//al4 = new ArrayList<String>(CollectionUtils.subtract(al, al2));
			al5 = new ArrayList<String>(CollectionUtils.subtract(al2, al));
				
		
		/*System.out.println("Difference in elements is "+al4.size());
		for(String el:al5) {			
		System.out.println("The element differed is "+el);
		}*/
		
		int newElems = al5.size();
		//System.out.println("First screen only elements are "+al4.size());
		System.out.println("Next screen only elements are  "+newElems);
		
		
		
		if(newElems==0) {
				System.out.println("Total no of playlist elements is "+Total);			
				// Verify with the expected count
				Total = (newElems+FirstScrElems);
				return;
			
			}else{			
			for(String el:al5) {			
				System.out.println("This is the element you find when you swipe down "+el);				
				}
			Total = (newElems+FirstScrElems);
			FirstScrElems=Total;	
			}	
			
		al=al2;
			
		}
		
		Assert.assertEquals(PLItems, FirstScrElems);
		
	/*	if(al5.size()!=0) {			
			System.out.println("Total no of elements = "+Total);				
			KeywordDriver.SwipeDown(driver, paramList);
			Thread.sleep(3000);			
			al=al2;
		}else {			
			System.out.println("Total no of playlist elements is "+Total);			
			// Verify with the expected count
			Assert.assertEquals(PLItems, Total);
			return;
		}*/
		
		/*if(al.equals(al2)) {
			System.out.println("No new items found below after swiping down.");
		}else{
			System.out.println("There are new items below");
			ArrayList<String> al3 = new ArrayList<String>();
			//System.out.println(al.remove(al2));
		}*/
	}
	
	public static void DragAndDrop(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException{		
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String SchoolName= (String) paramList.get(SPECIAL_VALUE_FIELD1);	
		//String SchoolAddress= (String) paramList.get(SPECIAL_VALUE_FIELD2);

		String DrgDrpItems[] = locator.split(":");
		String DD1=DrgDrpItems[0];
		String DD2=DrgDrpItems[1];
		WebElement DragItem = driver.findElement(By.xpath(DD1));
		WebElement DropItem = driver.findElement(By.xpath(DD2));

		try{
		//Thread.sleep(2000);
		Actions actions = new Actions(driver);
		//actions.dragAndDrop(DragItem, DropItem).build().perform();
		actions.clickAndHold(DragItem).moveToElement(DropItem).release(DragItem).build().perform();
		Thread.sleep(2000);
		}catch(Exception Exc) {
			TouchAction action = new TouchAction((MobileDriver)driver);
			action.longPress(DragItem).moveTo(DropItem).release().perform(); 
			System.out.println("Tried touch actions class");
		}
	}
	
	public static void GetIndexAndClick(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException{		
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String value = (String) paramList.get(VALUE);
			
		
		List<WebElement> Playlists = driver.findElements(By.id(locator));
		for(int i=0;i<=Playlists.size();i++) {
			
			String name = Playlists.get(i).getAttribute("text");
			System.out.println(name);
			if (name.equals(value)) {
				
				//Increase the value of i by 1
				i++;				
				String ItemToClick = "(//*[@class='android.widget.CheckBox'])["+i+"]";
				System.out.println(ItemToClick);				
				driver.findElement(By.xpath(ItemToClick)).click();
				return;
			}
		}		
	}
	
	
	public static void navigateToOrgHome(WebDriver driver, ArrayList<Object> paramList) {
		System.out.println("################## Clean Up before executing the next test case ####################");
		
		WebDriverWait wait = new WebDriverWait(driver, 30);		
		try {
		driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/openMenu")).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.id("com.gadgetsoftware.android.gadgetlabs:id/llbackToHome")));
		driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/llbackToHome")).click();	
		}catch(NoSuchElementException e) {
			System.out.println("Organization menu is not displayed.");
			KeywordDriver.vPubLevelCleanUp(driver, paramList);
		}
	}
	
	public static void OrgLevelCleanUp(WebDriver driver, ArrayList<Object> paramList)  {		
		
		System.out.println("******** Triggerring the recovery **********");		
		
		try {
			driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/backArrow")).click();
			KeywordDriver.navigateToOrgHome(driver, paramList);	
		}catch(NoSuchElementException E) {				
				try {
					// Click the close button on the vPub Summary screen
					driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/ivCloseInfoPopUp")).click();
					KeywordDriver.navigateToOrgHome(driver, paramList);	
				}catch(NoSuchElementException e2) {
					try {
						if(driver.findElement(By.id("android:id/home")).isDisplayed()) {
							System.out.println("App is on the vPub menu screen");							
						}
					}catch(NoSuchElementException NSElem) {						
						try {
							if(driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/homeMenusListView")).isDisplayed()) {
								System.out.println("At the vPub level");
								KeywordDriver.vPubLevelCleanUp(driver, paramList);
							}
						}catch(NoSuchElementException NoSuEl) {
							if(driver.findElement(By.id("com.gadgetsoftware.android.gadgetlabs:id/homeMenusListView")).isDisplayed()) {
								System.out.println("At the vPub level");
								KeywordDriver.vPubLevelCleanUp(driver, paramList);
							}
						}
						}
					}
									
				}
		}
	
	
	public static void vPubLevelCleanUp(WebDriver driver, ArrayList<Object> paramList)  {	
			
		System.out.println("User might be at the vPub level");
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		
		try {				
				// Navigate back to the vPub home screen by clicking the back button till the exit confirmation popup appears
				while(true) {
					int count = 0;
					driver.navigate().back();
					count++;
					try {
							if(driver.findElement(By.id("android:id/button2")).isDisplayed()) {
								driver.findElement(By.id("android:id/button2")).click();						
								System.out.println("Went back "+count+" time!. But we are on the vPub home page!");
								break;
							}else if(count==4){
								System.out.println("Tried "+count+"times!. Thats all for now. Check for popups or webviews or some other elements on the page that might be preventing the app to go back.");
								break;
							}						
					
					}catch(Exception E) {
								System.out.println("************* You must be somewhere else! May be out of the app! *****************");
								break;
							}
						
				}				
				
			}catch(Exception Exc) {
				
				System.out.println("You will see this msg when the app wasn't able to navigate back to vPub home.");
			}
			
		}
	

}

		
