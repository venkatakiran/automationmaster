package com.tss.hybridFrameWork;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
//import io.appium.java_client.ScrollsTo;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.tss.utils.OpenDialog;

public class KWDriverTest {
	
	static final int IDENTIFYBY = 0;
	static final int SPECIAL_VALUE_FIELD=0;
	static final int URL =0;
	static final int LOCATOR = 1;
	static final int VALUE = 2;
	static final int XPATH1 = 1;
	static final int XPATH2 = 2;
	static final int XPATH3 = 3;
	
			
	
	
	public static void AcceptPopup(WebDriver driver, ArrayList<Object>  paramList){
		
		driver.switchTo().alert().accept();
		
		
	}
	
	public static void DismissPopup(WebDriver driver, ArrayList <Object>  paramList) {
		
		driver.switchTo().alert().dismiss();
		
	}
	
	
	public static void VerifyPopupText(WebDriver driver, ArrayList <Object>  paramList){
		String valuetoType = (String) paramList.get(SPECIAL_VALUE_FIELD);
		
		driver.switchTo().alert().getText().contains(valuetoType);
		
	}
	
	public static void wait(WebDriver driver, ArrayList <Object>  paramList){
		
		int waitTime = Integer.parseInt((String)paramList.get(SPECIAL_VALUE_FIELD));
		try {
			Thread.sleep(waitTime * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//driver.manage().timeouts().implicitlyWait(waitTime,TimeUnit.SECONDS);
	}
	
	public static void waitExplicit(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
			
		if (identifyBy.equalsIgnoreCase("id")){
		WebDriverWait wait = new WebDriverWait(driver, 40);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
	    }else if (identifyBy.equalsIgnoreCase("xpath")){
			WebDriverWait wait = new WebDriverWait(driver, 40);
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(locator)));
		    }else if (identifyBy.equalsIgnoreCase("class")){
		    	WebDriverWait wait = new WebDriverWait(driver, 40);
		    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
		    	}else if (identifyBy.equalsIgnoreCase("name")){
			    	WebDriverWait wait = new WebDriverWait(driver, 40);
			    	wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(locator)));
			    	}
		}
	
	public static void waitForAbsence(WebDriver driver,ArrayList<Object> paramList) {
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoType = (String) paramList.get(VALUE);
		
		
		if (identifyBy.equalsIgnoreCase("id")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(locator)));
		} else if (identifyBy.equalsIgnoreCase("xpath")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
		} else if (identifyBy.equalsIgnoreCase("class")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className(locator)));
		} else if (identifyBy.equalsIgnoreCase("name")) {
			WebDriverWait wait = new WebDriverWait(driver, 40);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.name(locator)));
		}
	}

	public static void navigateto(WebDriver driver, ArrayList <Object>  paramList){
		driver.get((String)paramList.get(URL));
		driver.manage().window().maximize();
	}
	
	
	public static void clickButton(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){			
			WebElement WE=driver.findElement(By.xpath(locator));
			WE.click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			WebElement WE=driver.findElement(By.id(locator));
			WE.click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			WebElement WE=driver.findElement(By.name(locator));
			WE.click();
		}else if (identifyBy.equalsIgnoreCase("class")){
			WebElement WE=driver.findElement(By.className(locator));
			WE.click();
		}else if (identifyBy.equalsIgnoreCase("label")){
			WebElement WE=driver.findElement(By.partialLinkText(locator));
			WE.click();
	}}

	public static void clickLink(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("link")){
			driver.findElement(By.linkText(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("plink")){
			driver.findElement(By.partialLinkText(locator)).click();
		}
	}
	
	
	public static void JSEClickElement(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			WebElement element = driver.findElement(By.xpath(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
			
		}else if (identifyBy.equalsIgnoreCase("id")){
			WebElement element = driver.findElement(By.id(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
			
		}else if (identifyBy.equalsIgnoreCase("name")){
			WebElement element = driver.findElement(By.name(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			//executor.executeScript("arguments[0].click();", element);
			executor.executeScript("mobile: tap", element);
		}
	}
	
	
	public static void JSETypeInEditBox(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException{
		//String identifyBy = (String) paramList.get(IDENTIFYBY);
		//String locator = (String) paramList.get(LOCATOR);
		
		{
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("tinyMCE.activeEditor.setContent('<h1>This is a simple description</h1> Testing')");
			
			/*Thread.sleep(3000);
			WebElement iframeItem = driver.findElement(By.xpath("//*[@id='item-description_ifr'] | //*[@id='location-description_ifr']"));
			driver.switchTo().frame(iframeItem);
			Thread.sleep(2000);
			
			WebElement editorbody = driver.findElement(By.className("mce-content-body"));			
			executor.executeScript("arguments[0].innerHTML = '<p>This is a sample text created by Kiran.<p>'", editorbody);
			
			driver.findElement(By.cssSelector("body")).sendKeys("your data");
			
			driver.switchTo().defaultContent();
			Thread.sleep(2000);*/
		}
	}
	
	
	public static void EnterText(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType);
		}
	}
	
	
	
	public static void EnterTextAndHitReturn(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
			//driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType,Keys.RETURN);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType,Keys.RETURN);
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType, Keys.RETURN);
		}
	}
	
	public static void EnterTextAndHitTab(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType,Keys.TAB);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType,Keys.TAB);
		}
	}
	
	public static void isTextPresent(WebDriver driver, ArrayList<Object>  paramList) {
		String identifyBy = (String) paramList.get(SPECIAL_VALUE_FIELD);
		driver.findElement(By.xpath("//*[contains(.,'"+ identifyBy +"')]")).getAttribute("label");
			
	}
	
	
	public static void verifyTextPlain(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		valuetoVerify.trim().toLowerCase();	
		
		if (identifyBy.equalsIgnoreCase("xpath")){		
		String ElementName = driver.findElement(By.xpath(locator)).getAttribute("label");
		ElementName.toLowerCase();
		System.out.println("****************************************"+ElementName+"**********************************************");			
		String[] ElemName2 = ElementName.split("");				
		String[] valueToVerify2 = valuetoVerify.split("");		
		boolean result = Arrays.equals(valueToVerify2,ElemName2);				
		if(result == false){
            throw new Exception("************************************Text doesn't match**************************************");
        }
		}else if (identifyBy.equalsIgnoreCase("name")){			
			String ElementName = driver.findElement(By.name(locator)).getAttribute("label");
			ElementName.toLowerCase();
			System.out.println("****************************************"+ElementName+"**********************************************");				
			String[] ElemName2 = ElementName.split("");					
			String[] valueToVerify2 = valuetoVerify.split("");			
			boolean result = Arrays.equals(valueToVerify2,ElemName2);					
			if(result == false){
	            throw new Exception("************************************Text doesn't match**************************************");
	        }
			}
	}
	
	
	public static void verifyValue(WebDriver driver, ArrayList<Object>  paramList) throws Exception{
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		valuetoVerify.trim().toLowerCase();	
		
		if (identifyBy.equalsIgnoreCase("xpath")){		
		String ElementName = driver.findElement(By.xpath(locator)).getAttribute("value");
		ElementName.trim().toLowerCase();
		System.out.println("****************************************"+ElementName+"**********************************************");			
		String[] ElemName2 = ElementName.split("");				
		String[] valueToVerify2 = valuetoVerify.split("");		
		boolean result = Arrays.equals(valueToVerify2,ElemName2);				
		if(result == false){
            throw new Exception("************************************Text doesn't match**************************************");
        }
		}else if (identifyBy.equalsIgnoreCase("name")){			
			String ElementName = driver.findElement(By.name(locator)).getAttribute("value");
			ElementName.trim().toLowerCase();
			System.out.println("****************************************"+ElementName+"**********************************************");				
			String[] ElemName2 = ElementName.split("");					
			String[] valueToVerify2 = valuetoVerify.split("");			
			boolean result = Arrays.equals(valueToVerify2,ElemName2);					
			if(result == false){
	            throw new Exception("************************************Text doesn't match**************************************");
	        }
			}
	}
	
	public static void verifyElement(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).isDisplayed();
		}
	}
	
	
	public static void verifyElementNotExist(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoVerify = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
		Assert.assertTrue(driver.findElements(By.xpath(locator)).size()<1);
		}		
		else if (identifyBy.equalsIgnoreCase("id")){
			Assert.assertTrue(driver.findElements(By.id(locator)).size()<1);	
		}else if (identifyBy.equalsIgnoreCase("name")){
			Assert.assertTrue(driver.findElements(By.name(locator)).size()<1);	
		}
	
	}
	
	public static void verifyElementCount(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		
		int count = Integer.parseInt(valuetoVerify);
		if (identifyBy.equalsIgnoreCase("xpath")){
		    //RemoteWebDriver rmtDriver = (RemoteWebDriver) driver;		
			List<WebElement> rows = driver.findElements(By.xpath(locator));
			int records = rows.size(); 
			Assert.assertEquals(count, records);
		}else if (identifyBy.equalsIgnoreCase("name")){
			List<WebElement> rows = driver.findElements(By.name(locator));
			int records = rows.size(); 
			Assert.assertEquals(count, records);
		}else if(identifyBy.equalsIgnoreCase("class")){
			List<WebElement> rows = driver.findElements(By.className(locator));
			int records = rows.size(); 
			Assert.assertEquals(count, records);
		}					
	}
	

	public static void selectRadiobutton(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}

	}

	public static void selectCheckbox(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String checkFlag = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.xpath(locator)).isSelected())){
					driver.findElement(By.xpath(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("id")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.id(locator)).isSelected())){
					driver.findElement(By.id(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("name")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.name(locator)).isSelected())){
					driver.findElement(By.name(locator)).click();
				}
			}
		}
	}

	public static void selectValue(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valToBeSelected = (String) paramList.get(VALUE);
		WebElement option = null;
		
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			option = driver.findElement(By.xpath(locator));
		}else if (identifyBy.equalsIgnoreCase("id")){
			option = driver.findElement(By.id(locator));
		}else if (identifyBy.equalsIgnoreCase("name")){
			option = driver.findElement(By.name(locator));
		}else if (identifyBy.equalsIgnoreCase("class")){
			option = driver.findElement(By.className(locator));
		}
		
		//option.selectByVisibleText(valToBeSelected);
		
		//option.sendKeys(valToBeSelected);
		
				
       List <WebElement> options = driver.findElements(By.tagName("option"));
		for (WebElement option1 : options) {
			if (valToBeSelected.equalsIgnoreCase(option1.getText())){
				option1.click();
			}
		}
	}
				
	

	public static void enterKeyPress(WebDriver driver, ArrayList <Object>  paramList) throws AWTException, InterruptedException{
		
		
		Robot robot = new Robot();
		/*robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);*/
		Thread.sleep(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
	}
	
	public static void scrollToElement(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
		String value = (String) paramList.get(SPECIAL_VALUE_FIELD);
		
		/*((IOSElement) driver).scrollTo(value);
		Thread.sleep(2000);*/
		 
		 //WebElement ScrollElement = driver.findElement(MobileBy.IosUIAutomation("new UiScrollable(new UiSelector()).scrollIntoView("+"new UiSelector().text(\"Personal Details\"\"));"));
		 //ScrollElement.getLocation();
		
		//Get the size of screen.
		Dimension size;
		  size = driver.manage().window().getSize();
		  System.out.println(size);
		   
		  //Find swipe start and end point from screen's width and height.
		  //Find starty point which is at bottom side of screen.
		  int starty = (int) (size.height * 0.80);
		  //Find endy point which is at top side of screen.
		  int endy = (int) (size.height * 0.20);
		  //Find horizontal point where you wants to swipe. It is in middle of screen width.
		  int startx = size.width / 2;
		  System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

		  //Swipe from Bottom to Top.
		  /*((AppiumDriver) driver).swipe(startx, starty, startx, endy, 3000);
		  Thread.sleep(2000);*/
		  //Swipe from Top to Bottom.
		  ((AppiumDriver) driver).swipe(startx, endy, startx, starty, 3000);
		  Thread.sleep(2000);
	}
	
	public static void GoBack(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException {
		//String value = (String) paramList.get(SPECIAL_VALUE_FIELD);
		
		driver.close();

	}
	
	public static void TapOnCoordinates(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		String value1 =  (String) paramList.get(SPECIAL_VALUE_FIELD);	
			
		String[] StrArr=value1.split(",");		
		String a1 = null;
		String a2 = null;		
		Double Ad = null;
		Double Bd = null;
		
		a1= StrArr[0];
		a2=StrArr[1];				
				
		Ad = Double.parseDouble(a1);
		Bd = Double.parseDouble(a2);	
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
	    HashMap<String, Double> tapObject = new HashMap<String, Double>();
	    tapObject.put("x",  Ad); // in pixels from left
	    tapObject.put("y",  Bd); // in pixels from top        
				
		js.executeScript("mobile: tap", tapObject);
	    Thread.sleep(2000);
	}

	
		public static void SwipeLeft(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		Dimension size = driver.manage().window().getSize(); 	                 
				int startx = (int) (size.width * 0.70);
				int endx = (int) (size.width * 0.20);
				int starty = size.height / 3; 
			    ((IOSDriver) driver).swipe(startx, starty, endx, starty, 1000);
			    }
		
		
		public static void SwipeRight(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		Dimension size = driver.manage().window().getSize(); 	                 
			     int startx = (int) (size.width * 0.1);
			     int endx = (int) (size.width * 0.9);
			     int starty = size.height / 2; 
			     ((IOSDriver) driver).swipe(startx, starty, endx, starty, 2000);
			     }
		
}