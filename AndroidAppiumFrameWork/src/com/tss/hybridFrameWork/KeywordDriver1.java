package com.tss.hybridFrameWork;

import java.awt.AWTException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.tss.utils.OpenDialog;

public class KeywordDriver1 {
	
	static final int IDENTIFYBY = 0;
	static final int SPECIAL_VALUE_FIELD=0;
	static final int URL =0;
	static final int LOCATOR = 1;
	static final int VALUE = 2;
	static final int XPATH1 = 1;
	static final int XPATH2 = 2;
	static final int XPATH3 = 3;
	static final int XPATH4 = 4;
	static final int XPATH5 = 5;
	static final int XPATH6 = 6;
	static final int XPATH7 = 7;
	static final int XPATH8 = 8;
	static final int XPATH9 = 9;
	static final int XPATH10 = 10;
	static final int XPATH11 = 11;
	static final int XPATH12 = 12;
	
			
	
	
	public static void closeJscriptPopup(WebDriver driver, Alert alert){
		alert = driver.switchTo().alert();
		alert.accept();
	}
	
	public static void wait(WebDriver driver, ArrayList <Object>  paramList){
		
		int waitTime = Integer.parseInt((String)paramList.get(SPECIAL_VALUE_FIELD));
		try {
			Thread.sleep(waitTime * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//driver.manage().timeouts().implicitlyWait(waitTime,TimeUnit.SECONDS);
	}

	public static void navigateto(WebDriver driver, ArrayList <Object>  paramList){
		//driver.get((String)paramList.get(URL));
		driver.navigate().to("http://qa2.beeonics.net");
		driver.manage().window().maximize();
	}
	public static void clickButton(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("text")){
			driver.findElement(By.tagName(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}
	}

	public static void clickLink(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("link")){
			driver.findElement(By.linkText(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("plink")){
			driver.findElement(By.partialLinkText(locator)).click();
		}
	}
	
	
	public static void JSEClickElement(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			WebElement element = driver.findElement(By.xpath(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			
		}else if (identifyBy.equalsIgnoreCase("id")){
			WebElement element = driver.findElement(By.id(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
			
		}else if (identifyBy.equalsIgnoreCase("name")){
			WebElement element = driver.findElement(By.name(locator));
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", element);
		}
	}
	
	
	public static void JSETypeInEditBox(WebDriver driver, ArrayList<Object>  paramList) throws InterruptedException{
		//String identifyBy = (String) paramList.get(IDENTIFYBY);
		//String locator = (String) paramList.get(LOCATOR);
		
		{
			
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("tinyMCE.activeEditor.setContent('<h1>This is a simple description</h1> Testing')");
			
			/*Thread.sleep(3000);
			WebElement iframeItem = driver.findElement(By.xpath("//*[@id='item-description_ifr'] | //*[@id='location-description_ifr']"));
			driver.switchTo().frame(iframeItem);
			Thread.sleep(2000);
			
			WebElement editorbody = driver.findElement(By.className("mce-content-body"));			
			executor.executeScript("arguments[0].innerHTML = '<p>This is a sample text created by Kiran.<p>'", editorbody);
			
			driver.findElement(By.cssSelector("body")).sendKeys("your data");
			
			driver.switchTo().defaultContent();
			Thread.sleep(2000);*/
		}
	}
	
	
	public static void typeinEditbox(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).clear();
			driver.findElement(By.id(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType);
		}

	}
	
	public static void verifyText(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).getText().equalsIgnoreCase(valuetoVerify);
			//driver.getPageSource().compareToIgnoreCase(valuetoVerify);
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.xpath(locator)).getText().equalsIgnoreCase(valuetoVerify);
			//driver.getPageSource().contains(valuetoVerify);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.xpath(locator)).getText().equalsIgnoreCase(valuetoVerify);
			//driver.getPageSource().contains(valuetoVerify);
		}
	}
	
	/*public static void verifyTextPresent(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoVerify = (String) paramList.get(VALUE);
		
				
		if (identifyBy.equalsIgnoreCase("xpath")){
			Assert.assertTrue(driver.findElements(By.xpath(locator)).size()<1);
			}		
			else if (identifyBy.equalsIgnoreCase("id")){
				Assert.assertTrue(driver.findElements(By.id(locator)).size()<1);	
			}else if (identifyBy.equalsIgnoreCase("name")){
				Assert.assertTrue(driver.findElements(By.name(locator)).size()<1);	
			}
	}*/
	
	public static void verifyElement(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).isDisplayed();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).isDisplayed();
		}
	}
	
	
	public static void verifyElementNotExist(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		//String valuetoVerify = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
		Assert.assertTrue(driver.findElements(By.xpath(locator)).size()<1);
		}		
		else if (identifyBy.equalsIgnoreCase("id")){
			Assert.assertTrue(driver.findElements(By.id(locator)).size()<1);	
		}else if (identifyBy.equalsIgnoreCase("name")){
			Assert.assertTrue(driver.findElements(By.name(locator)).size()<1);	
		}
	
	}
	
	
	public static void selectRadiobutton(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).click();
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).click();
		}

	}

	public static void selectCheckbox(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String checkFlag = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.xpath(locator)).isSelected())){
					driver.findElement(By.xpath(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("id")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.id(locator)).isSelected())){
					driver.findElement(By.id(locator)).click();
				}
			}
		}else if (identifyBy.equalsIgnoreCase("name")){
			if ((checkFlag).equalsIgnoreCase("ON")){
				if (!(driver.findElement(By.name(locator)).isSelected())){
					driver.findElement(By.name(locator)).click();
				}
			}
		}
	}

	public static void selectValue(WebDriver driver, ArrayList <Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valToBeSelected = (String) paramList.get(VALUE);
		WebElement option = null;
		
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			option = driver.findElement(By.xpath(locator));
		}else if (identifyBy.equalsIgnoreCase("id")){
			option = driver.findElement(By.id(locator));
		}else if (identifyBy.equalsIgnoreCase("name")){
			option = driver.findElement(By.name(locator));
		}else if (identifyBy.equalsIgnoreCase("class")){
			option = driver.findElement(By.className(locator));
		}
		
		//option.selectByVisibleText(valToBeSelected);
		
		//option.sendKeys(valToBeSelected);
		
				
       List <WebElement> options = driver.findElements(By.tagName("option"));
		for (WebElement option1 : options) {
			if (valToBeSelected.equalsIgnoreCase(option1.getText())){
				option1.click();
			}
		}
	
	}
	
	
	public static void EnterText(WebDriver driver, ArrayList<Object>  paramList){
		String identifyBy = (String) paramList.get(IDENTIFYBY);
		String locator = (String) paramList.get(LOCATOR);
		String valuetoType = (String) paramList.get(VALUE);
		
		if (identifyBy.equalsIgnoreCase("xpath")){
			driver.findElement(By.xpath(locator)).clear();
			driver.findElement(By.xpath(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("name")){
			driver.findElement(By.name(locator)).clear();
			driver.findElement(By.name(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("class")){
			driver.findElement(By.className(locator)).clear();
			driver.findElement(By.className(locator)).sendKeys(valuetoType);
		}else if (identifyBy.equalsIgnoreCase("id")){
			driver.findElement(By.id(locator)).clear();
			driver.findElement(By.id(locator)).sendKeys(valuetoType);
		}
	}		
			
		
		
				
	
	
public static void fileUpload(WebDriver driver, ArrayList <Object>  paramList) throws InterruptedException{
		
		String strFilePath = (String) paramList.get(SPECIAL_VALUE_FIELD);
		OpenDialog.setClipboardData(strFilePath);
		
		try {
			OpenDialog.selectFile();
		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
}




public static void notifySpecificUsers(WebDriver driver, ArrayList <Object>  paramList){
	
	String identifyBy = (String) paramList.get(IDENTIFYBY);
	String XP1 = (String) paramList.get(XPATH1);
	String XP2 = (String) paramList.get(XPATH2);
	String XP3 = (String) paramList.get(XPATH3);
	String XP4 = (String) paramList.get(XPATH4);
	String XP5 = (String) paramList.get(XPATH5);
	String XP6 = (String) paramList.get(XPATH6);
	String XP7 = (String) paramList.get(XPATH7);
	String XP8 = (String) paramList.get(XPATH8);
	String XP9 = (String) paramList.get(XPATH9);
	String XP10 = (String) paramList.get(XPATH10);
	String XP11 = (String) paramList.get(XPATH11);
	String XP12 = (String) paramList.get(XPATH12);
	
	
	System.out.println("Entering the try block");
	
	
	driver.findElement(By.xpath("//a[contains(.,'Select Locations')]")).click();
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	/*String XP1= "//*[@id='target-select-location-last-known']/div[1]/div[";
	String XP2="]/div[2]/div[";
	//String XP3= "//*[@id='target-select-location-last-known']/div[1]/div[1]/div[2]/div[";
	String XP3= "]/div[";
	String XP4="]/div/div/div[2]/div";
	String XP5 = "]/div/div/div[1]/div/input[1]";*/
	
	//*[@id='target-select-location-last-known']/div[1]/div[2]/div[2]/div[1]/div[3]
	
	
	//WebElement table = driver.findElement(By.className("location-group"));
   // List<WebElement> rows = driver.findElements(By.xpath("//*[@id='target-select-location-last-known']/div[1]"));
    // Iterate over rows to get each count
    
   // System.out.println(rows.size());
    
    for(int k=1;k<=2;k++){
    	
    	System.out.println("Entered K loop");
	
	for (int i=1; i<=3; i++) {
    	System.out.println("Entered outer for loop********");
    	
    	for (int j=1; j<=3; j++){
    		System.out.println("Entered Inner for loop########");
	        String pCount = driver.findElement(By.xpath(XP1+k+XP2+i+XP3+j+XP4)).getText();
	        System.out.println(pCount);
	        
	        if (pCount.equals("Football Field")) {
	        	
	        	driver.findElement(By.xpath(XP1+k+XP2+i+XP3+j+XP5)).click();

	        	// Click the Add button
	        	driver.findElement(By.xpath("//*[@id='target-select-location-last-known']/div[2]/div/a[1]")).click();
	        	        	       	
    		   	break;
	        } 
	        
    		}
		}
    }
    
    
	
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.findElement(By.xpath("//a[contains(.,'Select Organizations')]")).click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*String XP6= "//*[@id='target-select-business-units']/div[1]/div[";
		String XP7="]/div[";
		
		String XP8= "]/div/div/div[2]/div";
		
		String XP9 = "]/div/div/div[1]/div/input[1]";*/
		
		//*[@id='target-select-location-last-known']/div[1]/div[2]/div[2]/div[1]/div[3]
		
		
		//WebElement table = driver.findElement(By.className("location-group"));
	   // List<WebElement> rows = driver.findElements(By.xpath("//*[@id='target-select-location-last-known']/div[1]"));
	    // Iterate over rows to get each count
	    
	   // System.out.println(rows.size());
	    
	   /* for(int k=1;k<=2;k++){
	    	
	    	System.out.println("Entered K loop");*/
		
		for (int i=1; i<=9; i++) {
	    	System.out.println("Entered outer for loop********");
	    	
	    	for (int j=1; j<=2; j++){
	    		System.out.println("Entered Inner for loop########");
		        String pCount = driver.findElement(By.xpath(XP6+i+XP7+j+XP8)).getText();
		        System.out.println(pCount);
		        
		        if (pCount.equals("Cricket kit")) {
		        	
		        	driver.findElement(By.xpath(XP6+i+XP7+j+XP9)).click();
		        	
		        	// Click the Add button
		        	driver.findElement(By.xpath("//*[@id='target-select-business-units']/div[2]/div/a[1]")).click();
		        	        	       			
        		   	break;
		        } 
		        
	    		}
			}
	    //}




	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	driver.findElement(By.xpath("//a[contains(.,'Select Roles')]")).click();
	try {
		Thread.sleep(2000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	/*String XP10= "//*[@id='target-select-roles']/div[2]/div[";
	String XP11="]/div/div[2]/div";
	String XP12 = "]/div/div[1]/div/input[1]";*/
	
	
	
	 	for (int j=1; j<=3; j++){
    		System.out.println("Entered Inner for loop########");
	        String pCount = driver.findElement(By.xpath(XP10+j+XP11)).getText();
	        System.out.println(pCount);
	        
	        if (pCount.equals("Signed-Up User")) {
	        	
	        	driver.findElement(By.xpath(XP10+j+XP12)).click();
	        	// Click the Add button
	        	driver.findElement(By.xpath("//*[@id='target-select-roles']/div[3]/div/a[1]")).click();
	        	
	        	        	       	
    		   	break;
	        } 
	        
    		}
		
	

}
			
}
		
		
		
	
	








