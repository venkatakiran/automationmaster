package com.tss.utils;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;


import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
public class TestUtils {
	static public String  dateTimeNow(String strFormat){
		String str = null;
		
		try {
		SimpleDateFormat dateFormat = new SimpleDateFormat(strFormat);
		//get current date time with Date()
		str = dateFormat.format(new Date());
		}
		catch (Exception e){
			  
			   str = SimpleDateFormat.getDateTimeInstance().format(new Date());
		 }	
		
		return str;
	}
	
	static public  void captureScreennShot(WebDriver driver, String strFileName){
		
		try {
			File scrnsht = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrnsht, new	File(strFileName));
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
