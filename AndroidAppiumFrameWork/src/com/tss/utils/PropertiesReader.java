package com.tss.utils;



import java.util.Locale;
import java.util.ResourceBundle;

import jxl.read.biff.BiffException;




public class PropertiesReader {

	private ResourceBundle bundle;
	
	
	public PropertiesReader(String propertyName ){
		bundle = ResourceBundle.getBundle(propertyName,  Locale.getDefault());
	}
	
	public String[]getProperties(String objName){
		
		String strProperty = bundle.getString(objName);
		return strProperty.split(";");
				
	}
	public String getProperty(String objName){
			return  bundle.getString(objName);
	}
	
	/* static public void main(String [] args){
		
		String[] str;
		
		XLSReader xlsReader = new XLSReader();
		try {
			xlsReader.openWorkBook("D:\\core.xls");
			xlsReader.setActiveSheet("Suite");
			ArrayList <String > arr = xlsReader.getTableHeader();
						
			//Hashtable <String, String> dataTable = xlsReader.readRow(2);
			//for (int i=0; i < arr.size(); i++){
			//	System.out.println("Column header is: " + arr.get(i) + "Value is : " + dataTable.get(arr.get(i)));
			//}
			int nRow = xlsReader.getRowCount()- 1;
			for (int i = 0; i < nRow; i++){
				Hashtable <String, String> dataTable = xlsReader.readRow((i+1));
				
				xlsReader.setActiveSheet(dataTable.get("TCID"));
				ArrayList <String > arr1 = xlsReader.getTableHeader();
				
				for (int j = 0; j < xlsReader.getRowCount()- 1; j++){
					Hashtable <String, String> dataTable1 = xlsReader.readRow((j+1));
					for (int z=0; z < arr1.size(); z++){
						System.out.println("Column header is: " + arr1.get(z) + "Value is : " + dataTable1.get(arr1.get(z)));
					}
					
				}
				xlsReader.setActiveSheet("Suite");
				
				
				
			}
			
		} catch (BiffException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		try {
			ObjectProperties prt = new ObjectProperties("config.object");
			str = prt.getObjectProperties("Yogi");
			System.out.println("Property 1" + str[0] + "Property 2 " + str[1]);
		}
		catch (MissingResourceException e) {
			
			System.out.println(e.getMessage());
		}
		
	
	}
	*/
}
	  	
