package com.tss.utils;

import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;

public class StartStopAppium {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ExecuteException 
	 * @throws InterruptedException 
	 */
	
	// CODE TO START APPIUM SERVER
	public static void startAppium() throws ExecuteException, IOException, InterruptedException {
		
		CommandLine command = new CommandLine("G:/Appium Tool/AppiumForWindows-1.3.4.1/Appium/node.exe");
		command.addArgument("G:/Appium Tool/AppiumForWindows-1.3.4.1/Appium/node_modules/appium/bin/appium.js", false);
		command.addArgument("--address", false);
		command.addArgument("127.0.0.1");
		command.addArgument("--port", false);
		command.addArgument("4729");
		command.addArgument("--no-reset", false);
		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		DefaultExecutor executor = new DefaultExecutor();
		executor.setExitValue(1);
		executor.execute(command, resultHandler);
		
		Thread.sleep(5000);  
        System.out.println("Appium server started"); 	
        Thread.sleep(5000);
	}
	
	// CODE TO TERMINATE APPIUM
	
	public static void stopAppium() throws ExecuteException, IOException, InterruptedException {
		
		CommandLine command = new CommandLine("cmd");  
	    command.addArgument("/c");  
	    command.addArgument("Taskkill /F /IM node.exe");  
	  
	    DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();  
	    DefaultExecutor executor = new DefaultExecutor();  
	    executor.setExitValue(1);  
	    executor.execute(command, resultHandler);  
	     
	    System.out.println("Appium server stop"); 
		
		
		
	}
}
