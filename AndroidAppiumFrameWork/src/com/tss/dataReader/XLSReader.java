package com.tss.dataReader;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class XLSReader {
	
	private Sheet wrkSheet;
	private Workbook wrkBook;
	private ArrayList<String> tblHeader = new ArrayList<String>();

	public Workbook openWorkBook(String strFileName) throws BiffException, IOException{
		FileInputStream fileStream = new FileInputStream(strFileName);
		wrkBook = Workbook.getWorkbook(fileStream);
		return  wrkBook;
	}
	
	public void setActiveSheet(String sheetName){
		wrkSheet = wrkBook.getSheet(sheetName); 
		tblHeader.clear();
		getTableHeader();
	}
	
	//Returns the Cell value by taking row and Column values as argument
	public String readColumn(int column,int row)
	{
		return wrkSheet.getCell(column,row).getContents();
	}
	
	public Hashtable<String, String>  readRow(int row)
	{
		int clmCount = getColumnCount();
		Hashtable <String, String> dataTable = new Hashtable <String, String>();  
		
		String strValue;
		
		for (int col = 0; col < clmCount; col++){
			strValue = readColumn(col,row);
			dataTable.put(tblHeader.get(col), strValue);
		}
		return dataTable;
	}
 
	
	public ArrayList<String> getTableHeader()
	{
		
		int clmCount = getColumnCount();
		if(tblHeader.isEmpty()){
					
			for(int col=0; col < clmCount; col++)
			{
				tblHeader.add(readColumn(col,0));
			}
		}
		return tblHeader;
	}
 
		
	public String getActiveSheetName(){
		return wrkSheet.getName();
	}
	public boolean isActiveSheet(String strSheetName){
		return wrkSheet.getName().equalsIgnoreCase(strSheetName);
	}
	
	public int getRowCount(){
		return wrkSheet.getRows(); 
	}
	public int getColumnCount(){
		return wrkSheet.getColumns(); 
	}

}
