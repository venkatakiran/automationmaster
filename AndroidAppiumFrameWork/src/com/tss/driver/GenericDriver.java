package com.tss.driver;

import org.openqa.selenium.WebDriver;


abstract public class GenericDriver {
	
	public  WebDriver getDriverInstance(){
		WebDriver driver = null;
		 Class theClass = null;
			try {
				theClass = Class.forName("org.openqa.selenium.firefox.FirefoxDriver");
				driver = (WebDriver)theClass.newInstance();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    catch (InstantiationException e) {
					e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			  		
		return driver;
	}

}
