package com.tss.driver;

import com.tss.utils.PropertiesReader;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;


public class IOSDriver extends GenericDriver{
	
	private String version;
	private String deviceType;
	private String bundleId;
	private String appPath;
	private String udId;
			
	private void initializeProperties(){
		PropertiesReader drvProperty;
		drvProperty = new PropertiesReader("com.tss.driver.config.iosdriver");
		version = drvProperty.getProperty("Version");
		deviceType = drvProperty.getProperty("DeviceType");
		bundleId = drvProperty.getProperty("BundleId");
		appPath = drvProperty.getProperty("AppPath");
		udId  = drvProperty.getProperty("UdId");
	}
	
	private void setCapabilities(DesiredCapabilities capabilities){
		
		capabilities.setCapability(CapabilityType.BROWSER_NAME, "iOS");
		capabilities.setCapability(CapabilityType.VERSION, version);
		capabilities.setCapability(CapabilityType.PLATFORM, "Mac");
		capabilities.setCapability("deviceName", deviceType);
		if (udId != null && !udId.isEmpty() && deviceType.equalsIgnoreCase("iPhone")){
			capabilities.setCapability("udid", udId);
		}
		capabilities.setCapability("platformName", "iOS");
		capabilities.setCapability("bundleid", bundleId);
		capabilities.setCapability("app", appPath);
	}
	
	@Override
	public WebDriver getDriverInstance(){
		 
		WebDriver drv = null;
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		initializeProperties();
		setCapabilities(capabilities);
		
		try {
			drv =  new RemoteWebDriver( new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return drv;
	}

}



