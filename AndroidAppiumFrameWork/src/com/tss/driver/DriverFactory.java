package com.tss.driver;

import org.openqa.selenium.WebDriver;

public class DriverFactory {
	
	static WebDriver driver = null;
		
	public static WebDriver getDriverInstance(String strDriverName){
			 
		Class theClass = null;
			try {
				theClass = Class.forName(("com.tss.driver." + strDriverName));
				GenericDriver drv = (GenericDriver) theClass.newInstance();
				driver = drv.getDriverInstance();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		    catch (InstantiationException e) {
					e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  		
			return driver;
	}

}


