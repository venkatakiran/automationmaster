package com.tss.driver;

import com.tss.utils.PropertiesReader;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;


public class AndroidAppiumDriver extends GenericDriver{
	
	private String deviceType;
	private String deviceName;
	private String appPath;
	private String udId;
	private String appActivity;
	private String appPackage;
	
	private void initializeProperties(){
		PropertiesReader drvProperty;
		drvProperty = new PropertiesReader("com.tss.driver.config.androidappiumdriver");
		drvProperty.getProperty("Version");
		deviceType = drvProperty.getProperty("DeviceType");
		drvProperty.getProperty("AppPackage");
		appActivity=drvProperty.getProperty("AppActivity");
		appPackage=drvProperty.getProperty("AppPackage");
		appPath = drvProperty.getProperty("AppPath");
		udId  = drvProperty.getProperty("UdId");
		deviceName  = drvProperty.getProperty("DeviceName");
	}
	
	private void setCapabilities(DesiredCapabilities capabilities){
		
		//capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");
        //capabilities.setCapability("device", deviceType);
        //capabilities.setCapability("platformVersion", version);
        capabilities.setCapability("platformName","Android");
        //capabilities.setCapability(CapabilityType.PLATFORM, "Android");
        capabilities.setCapability("deviceName", deviceName);
        
        /*if (udId != null && !udId.isEmpty() && deviceType.equalsIgnoreCase("Android")){
        	  capabilities.setCapability("device ID", udId);
		}*/
        capabilities.setCapability("app", appPath);      
        capabilities.setCapability("appPackage", appPackage);
        capabilities.setCapability("appActivity", appActivity);
        /*capabilities.setCapability(CapabilityType.SUPPORTS_FINDING_BY_CSS, true);
        capabilities.setCapability("autoWebview", true);
        capabilities.setCapability("setWebContentsDebuggingEnabled",true);*/
	}
	
	@Override
	public WebDriver getDriverInstance(){
		 
		AppiumDriver drv = null;
		DesiredCapabilities capabilities = new DesiredCapabilities();
		
		initializeProperties();
		setCapabilities(capabilities);
		
		try {
			drv = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		return drv;
	}

}



