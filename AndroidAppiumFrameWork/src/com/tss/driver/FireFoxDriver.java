package com.tss.driver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FireFoxDriver extends GenericDriver{
	@Override
	public WebDriver getDriverInstance(){
		WebDriver drv = null;
		drv = new FirefoxDriver();
		return drv;
	}

}
