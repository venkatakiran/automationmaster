package com.tss.testReport;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Date;

public class Report {
	boolean rptStatus = false;
	BufferedWriter bufferWriter ;
	FileWriter fileWriter;
	public boolean startReport(String fileName){
		
	   	try{
       	
    		fileWriter = new FileWriter(fileName);
    		bufferWriter = new BufferedWriter(fileWriter);
    		 
    	}catch(IOException e){
    		e.printStackTrace();
    	}
    	
    	return rptStatus;
	}
	
	public void startTestSuite(String strSuiteName){
		
		try {
			bufferWriter.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
			bufferWriter.newLine();
			bufferWriter.write("<Suite>");
			bufferWriter.newLine();
			bufferWriter.write("<Name>" + strSuiteName +"</Name>");
			bufferWriter.newLine();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	public void closeTestSuite(int passed, int failed, int skipped, String strStartTime, String strEndTime){
		
		try {
			bufferWriter.write("<TOTAL>" + (passed + failed + skipped) +"</TOTAL>");
			bufferWriter.newLine();
			bufferWriter.write("<PASSED>" +  passed  +"</PASSED>");
			bufferWriter.newLine();
			bufferWriter.write("<SKIPPED>" + skipped +"</SKIPPED>");
			bufferWriter.newLine();
			bufferWriter.write("<FAILED>" + failed  +"</FAILED>");
			bufferWriter.newLine();
			bufferWriter.write("</Suite>");
			bufferWriter.newLine();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	public void startTestCase(String strTestcaseID, String strTestDesc){
		
		try {
			bufferWriter.write("<TestCase ID=\"" + strTestcaseID +"\" "+ "DESC=\"" + strTestDesc + "\">");
			bufferWriter.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void closeTestCase(String strStartTime, String strEndTime, String strResult){
		try {
			bufferWriter.write("<Result>" + strResult +"</Result>");
			bufferWriter.newLine();
			bufferWriter.write("<StartTime>" + strStartTime +"</StartTime>");
			bufferWriter.newLine();
			bufferWriter.write("<EndTime>" + strEndTime +"</EndTime>");
			bufferWriter.newLine();
			bufferWriter.write("</TestCase>");
			bufferWriter.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void addTestStep(String strID, String strDesc, String strStartTime, String strEndTime, String strSCRNShot, String strResult){
		try {
			bufferWriter.write("<TestStep ID=\"" + strID +"\" "+ "DESC=\"" + strDesc + "\">");
			bufferWriter.newLine();
			bufferWriter.write("<Result>" + strResult +"</Result>");
			bufferWriter.newLine();
			bufferWriter.write("<StartTime>" + strStartTime +"</StartTime>");
			bufferWriter.newLine();
			bufferWriter.write("<EndTime>" + strStartTime +"</EndTime>");
			bufferWriter.newLine();
			bufferWriter.write("<ScreenShot>" + strSCRNShot +"</ScreenShot>");
			bufferWriter.newLine();
			bufferWriter.write("</TestStep>");
			bufferWriter.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void closeReport(){
			try {
				
				bufferWriter.flush();
				fileWriter.flush();
				bufferWriter.close();
				fileWriter.close();
				
				
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	
}


