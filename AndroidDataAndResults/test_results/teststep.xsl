<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="/" >


<xsl:apply-templates />



</xsl:template>


<xsl:template match="Suite/TestCase[@ID='Substitute Value']">

<table border="1">
<tr id="testcaseHeading" >
		<th>Teststep ID</th>
		<th>Description</th>
		<th>Start time</th>
		<th>End Time</th>
		<th>Result</th>
		<th>ScreenShot</th>
</tr>

<xsl:for-each select="TestStep">

<xsl:variable name="result">
	<xsl:choose>
        <xsl:when test="Result[.='Pass']">Pass</xsl:when>
        <xsl:otherwise>Fail</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<tr id= "{$result}">
		<td>
			
			<xsl:value-of select="@ID" />

		</td>
<td>
			
			<xsl:value-of select="@DESC" />

		</td>


<td>
			
			<xsl:value-of select="StartTime" />

		</td>
<td>
			
			<xsl:value-of select="EndTime" />
		</td>
<td>		
			<xsl:value-of select="Result" />

		</td>
		<td>
		
		<xsl:choose>
        <xsl:when test="ScreenShot[.='']"></xsl:when>
        <xsl:otherwise>
			<a href= "{ScreenShot}" target="_blank"> Screen Shot </a>
		</xsl:otherwise>
		</xsl:choose>
			

		</td>

		
	</tr>

</xsl:for-each>

</table>

</xsl:template>
<xsl:template match="TestCase" />
<xsl:template match="TestStep" />
<xsl:template match="Name" />
<xsl:template match="TOTAL" />
<xsl:template match="SKIPPED" />
<xsl:template match="PASSED" />
<xsl:template match="FAILED" />
</xsl:stylesheet> 