<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="/" >


<xsl:apply-templates />



</xsl:template>


<xsl:template match="Suite">

<div id="result">
<table border="1">


	<tr>

		<th>Suite Name</th>
		<td> 
			<xsl:value-of select="Name" /> 
		</td>
		
	</tr>
	<tr>

		<th>Total Test Case</th>
		<td> <xsl:value-of select="TOTAL" /> </td>
		
	</tr>
	<tr>

		<th id="totalTestPass">Pass Test Case</th>
		<td> <xsl:value-of select="PASSED" /> </td>
		
	</tr>
	<tr >
		<th id="totalTestFailed">Failed Test Case</th>
		<td> <xsl:value-of select="FAILED" /> </td>
		
	</tr>
	<tr>

		<th id="totalTestSkipped">Skipped Test Case</th>
		<td> <xsl:value-of select="SKIPPED" /> </td>
		
	</tr>

</table>

</div>

<div id="testcase">

<table border="1">
<tr id="testcaseHeading" >
		<th>TestCase ID</th>
		<th>Description</th>
		<th>Start time</th>
		<th>End Time</th>
		<th>Result</th>
</tr>
<xsl:apply-templates />

</table>

</div>

</xsl:template>


<xsl:template match="TestCase">

<xsl:variable name="result">
	 <xsl:value-of select="Result" />
</xsl:variable>

<xsl:variable name="name">
	 <xsl:value-of select="@ID" />
</xsl:variable>

	


<tr id= "{$result}">
		<td>
			
			<a href="#" id= "{$name}" onClick="AnchorClick(this.id); return false;"> <xsl:value-of select="@ID" />  </a>

		</td>
<td>
			
			<xsl:value-of select="@DESC" />

		</td>
		<td>
			
			<xsl:value-of select="StartTime" />

		</td>
		<td>
			
			<xsl:value-of select="EndTime" />

		</td>
<td>
			
			<xsl:value-of select="Result" />

		</td>
	</tr>





</xsl:template>	




<xsl:template match="Name" />
<xsl:template match="TOTAL" />
<xsl:template match="SKIPPED" />
<xsl:template match="PASSED" />
<xsl:template match="FAILED" />


</xsl:stylesheet> 